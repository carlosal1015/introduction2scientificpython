*****************************
introduction2scientificpython
*****************************
Supported tags
--------------
- `latest <https://gitlab.com/carlosal1015/introduction2scientificpython/container_registry>`_.

What is including
-----------------
This is a docker image that contains the following modules:

- `Python 3.8.5 (default, Jul 20 2020, 23:08:58) <https://www.python.org>`_.
- `NumPy 1.19.1 <https://numpy.org>`_.
- `Matplotlib 3.3.0 <https://matplotlib.org>`_.
- `Pandas 1.1.0 <https://pandas.pydata.org>`_.
- `IPython 7.16.1 <https://ipython.org>`_.
- `Jupyter 6.0.3 <https://jupyter.org>`_.
- `nbformat 5.0.7 <https://nbformat.readthedocs.io/en/latest>`_.
- `jupyter-core 4.6.3 <https://jupyter-core.readthedocs.io/en/latest>`_.
- `ipykernel 5.3.4 <https://github.com/ipython/ipykernel>`_.
- `ipywidgets 7.5.1 <https://ipywidgets.readthedocs.io/en/latest>`_.
- `nbconvert 5.6.1 <https://nbconvert.readthedocs.io/en/latest>`_.
- `traitlets 4.3.3 <https://traitlets.readthedocs.io/en/stable>`_.

Running the container
---------------------
Please, before to pull the image you must to `login <https://docs.gitlab.com/ee/user/packages/container_registry/#authenticating-to-the-gitlab-container-registry>`_.

.. code:: console

   [user@hostname ~]$ git clone https://gitlab.com/carlosal1015/introduction2scientificpython
   [user@hostname ~]$ cd introduction2scientificpython
   [user@hostname docker-scientific-python]$ docker-compose up -d

or

.. code:: console

   [user@hostname introduction2scientificpython]$ docker run -it --name introduction2scientificpython \
                                                     -v $(PWD):/notebooks \
                                                     -p 8888:8888 -d \
                                                     registry.gitlab.com/carlosal1015/introduction2scientificpython:latest

Open `localhost:8888 <http://localhost:8888>`_ and enjoy learning Python language.
