def to_celsius(t: float) -> float:
    '''Convert Fahrenheit to Celsius.'''
    return (t - 32.0) * 5.0 / 9.0


def above_freezing(t):
    if __name__ == "__main__":
        print("Archivo principal")
    else:
        print("Archivo secundario")
    return t > 0
