#!/usr/bin/python
# -*- coding: utf-8 -*-

first = 'world'
last = 'hello'

print(last + ' ' + first)

print(last, ' ', first)

print('"{0}", {1}'.format(last, first))
