#!/usr/bin/python
# -*- coding: utf-8 -*-

mylist = [7-2, 4*1.5, 6/3, 6/-3, 6//-3, 6 % 3, 6.0 % 3, 6 % 3.0, -6 %
          3, 6/-3.0, 2+4*3, (2+4)*3, 'a'+'b', 'a'+'3', 0 or 1, 0 and 1,
          2 or not 2]

for idx, val in enumerate(mylist):
    print(chr(idx + 97) + ')', val, type(val), sep='\t')
