#!/usr/bin/python
# -*- coding: utf-8 -*-

import quadrature
import numpy as np

help(quadrature)

mytry = quadrature.Integrator(np.exp, a=0.0, b=1.0, N=10)

print(
    f'The interval({mytry.a}, {mytry.b}) is divided into {mytry.N}
    subintervals.')
print('The nodal points are {0}'.format(mytry.x))

print(mytry.midpoint())

exactintegral = 1
errmidpoint = np.zeros(8)

for i in range(8):
    quad = quadrature.Integrator(lambda x: np.exp(x)*x, 0, 1, 10*pow(2, i))
    errmidpoint[i] = np.fabs(exactintegral - quad.midpoint())
    print(errmidpoint[i])

convratemid = np.log(errmidpoint[:-1]) / errmidpoint[1:] / np.log(2)
print("The convergence rates are ", convratemid)
