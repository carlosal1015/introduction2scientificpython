#!/usr/bin/python
# -*- coding: utf-8 -*-

# Primera parte
x, y = 1, 2
x = y
y = x + y

print("x is equal to {0} and y is equal to {1}".format(x, y))

# Segunda parte
x, y = 1, 2
x, y = y, x + y

print(f"x is equal to {x} and y is equal to {y}")

# Intercambiar dos números
a, b = 5, 9
a, b = b, a
