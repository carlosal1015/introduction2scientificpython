#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np


class Integrator:
    """A class to integrate functions numerically.
        Create an object by Integrator(f, a, b, N)
        f Integrand function
        a Interval lowerbound
        b Interval uperbound
        N Number of subintervals
    """

    def __init__(self, f, a, b, N):
        """(Integrator, f, a, b, N) -> NoneType
                Create an Integrator with N intervals
        """
        self. f = f
        self.a = a
        self.b = b
        self.N = N
        self.x = np.linspace(a, b, N + 1)
        self.h = (b - a) / N

    def midpoint(self):
        m = self.x[1:self.N+1] + self.x[0:self.N]
        y = self.f(m)
        T = self.h * np.sum(y)
        return T

# f, a, b, N are attributes from the object.
