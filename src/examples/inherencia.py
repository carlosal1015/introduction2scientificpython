class Organism(object):
    def __init__(self, name, x, y):
        self.name = name
        self.x = x
        self.y = y

    def __str__(self):
        return f"({self.name}[{self.x}, {self.y}])"

    def can_eat(self):
        return False

    def move(self):
        return None


class Arthropod(Organism):
    def __init__(self, name, x, y, legs):
        Organism.__init__(self, name, x, y)
        self.legs = legs

    def __str__(self):
        return f"({self.name}[{self.x}, {self.y}], {self.legs})"


blue_crab = Arthropod('Callinectes sapidus', 0, 0, 10)

print(blue_crab)
