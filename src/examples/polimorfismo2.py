class Rectangle(object):
    def __init__(self, x0, y0, x1, y1):
        self.x0, self.y0, self.x1, self.y1 = x0, y0, x1, y1

    def area(self):
        return (abs(self.x0 - self.x1)*abs(self.y0-self.y1))

    def contains(self, x, y):
        return (x <= abs(self.x0 - self.x1) and y <= abs(self.y0 - self.y1))

    def get_min_x(self):
        return min(self.x0, self.x1)

    def get_min_y(self):
        return min(self.y0, self.y1)

    def get_max_x(self):
        return max(self.x0, self.x1)

    def get_max_y(self):
        return max(self.y0, self.y1)


myRectangulo = Rectangle(0, 0, 2, 3)

print(myRectangulo.get_max_y())
