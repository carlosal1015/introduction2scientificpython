from math import exp


def mi_funcion(x):
    return (exp(x) + exp(-x)) / 2


print(mi_funcion(0))
