from logging import basicConfig, getLogger, info, debug, log, warning, error, critical, DEBUG, ERROR
from math import sqrt
LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
# Create and configure logger
basicConfig(filename = "/tmp/test.log", level = DEBUG, format = LOG_FORMAT, filemode = 'w')
logger = getLogger()

"""Test messages
debug("This is a harmless debug message.")
info("Our second message.")
warning("I'm sorry, but I can't do that, Dave.")
error("Did you just try to divide by zero?")
critical("The entire internet is down!!")
"""

def quadratic_formula(a, b, c):
    logger.info(f"quadratic_formula({a}, {b}, {c})")

    logger.debug("# Compute the discriminant")
    disc = b**2 - 4*a*c

    logger.debug("# Compute the roots")
    root1, root2 = (-b + sqrt(disc)) / (2*a), (-b - sqrt(disc)) / (2*a)

    logger.debug("# Return the roots")
    return (root1, root2)

roots = quadratic_formula(1, 0, -4)
print(roots)

print(logger.level)

from functools import lru_cache

@lru_cache(maxsize = 1000)
def fibonnaci(n):
    if type(n) != int:
        raise TypeError("n must be a positive integer.")
    if n < 1:
        raise ValueError("n must be a positive integer.")
    if n == 1:
        return 1
    elif n == 2:
        return 1
    elif n > 2:
        return fibonnaci(n-1) + fibonnaci(n-2)

for n in range(1, 51):
    print(fibonnaci(n + 1)/fibonnaci(n))
