import numpy as np


class Integrator:
    """
        A class to integrate functions numerically.
        Create an objet by integrator( f, a, b, N)
        f   Integrand function
        a= Interval lowerbound
        b= Interval upperbound
        N= Number of subintervals
    """

    def __init__(self, f, a, b, N):
        """
            (Integrator, a, b, N) -> NoneType
            Create an Integrator with N intervals,
        """
        self.a = a
        self.b = b
        self.N = N
        self.x = np.linspace(a, b, N + 1)
        self.h = (b - a) / N
        self.f = f

    def midpoint(self):
        """
        (Integrator) ->float
        Numerical integrattion by midpoint rule
        """
        m = 0.5*(self.x[1:self.N + 1] + self.x[0:self.N])
        y = self.f(m)
        T = self.h * np.sum(y)
        return T

    def trapezoidal(self):
        m = self.x[1:self.N]
        y = self.f(m)
        T = 0.5 * self.h * (self.f(self.a) + 2 * (np.sum(y)) + self.f(self.b))
        return T

    def simpson3_8(self):
        return 2/3 * self.midpoint() + 1/3 * self.trapezoidal()

    def simpson1_3(self):
        left = int((len(self.x) - 1) / 2 - 1)
        right = int((len(self.x) - 1) / 2)
        x_2j = [num for num in self.x[1:left+1] if num % 2 == 0]
        x_2j_1 = [num for num in self.x[1:right+1] if num % 2 == 1]
        return self.h / 3 * (self.f(self.a) + 2 * np.sum(self.f(x_2j)) + 4 * np.sum(self.f(x_2j_1)) + self.f(self.b))
