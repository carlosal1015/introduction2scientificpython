import numpy as np


class Integrator:
    """
    A class to integrate functions numerically.
    Create an objet by integrator( f, a, b, N)
    f   Integrand function
    a= Interval lowerbound
    b= Interval upperbound
    N= Number of subintervals
    """

    def __init__(self, f, a, b, N):
        """
        (Integrator, a, b, N) -> NoneType
        Create an Integrator with N intervals,
        """
        self.a = a
        self.b = b
        self.N = N
        self.x = np.linspace(a, b, N+1)     # TODO: part(a)
        self.h = (b-a) / N                  # TODO: part (b)
        self.f = f                          # TODO: Part (c)

    def midpoint(self):
        """
        (Integrator) ->float
        Numerical integrattion by midpoint rule
        """
        m = 0.5*(self.x[1:self.N+1]+self.x[0:self.N])           # TODO: part(a)
        y = self.f(m)                                           # TODO: part(b)
        T = self.h * np.sum(y)                                  # TODO: part(c)
        return T

    def trapez(self):
        """
        (Integrator) ->float
        Numerical integrattion by trapez rule
        """
        xleft = self.x[0:self.N]
        xright = self.x[1:self.N+1]
        y = 0.5*(self.f(xleft)+self.f(xright))
        T = self.h * np.sum(y)
        return T

    def simpson(self):
        """
        (Integrator) ->float
        Numerical integrattion by simpson rule
        """

        return (1/3)*self.trapez() + (2/3)*self.midpoint()
