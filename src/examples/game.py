import random

number = random.randint(1, 10)
niter = 0

print('Guess which number I chose between 1 to 10!')
while True:
    guess = int(input('Your guess:'))
    niter += 1

    if guess > number:
        print('Too high')
    elif guess < number:
        print('Too low')
    else:
        print(f'Needed {niter} iterations.')
        break
