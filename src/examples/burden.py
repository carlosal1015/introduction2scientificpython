import quadrature
import numpy as np

errmidpoint = errtrapezoidal = errsimpson3_8 = errsimpson1_3 = np.zeros(8)
exacintegral = 1.

for i in range(8):
    quad = quadrature.Integrator(lambda x: np.sin(x), 0, np.pi/2, 10*pow(2, i))
    errmidpoint[i] = np.fabs(exacintegral - quad.midpoint())
    print(errmidpoint[i])

convratemid = np.log(errmidpoint[:-1] / errmidpoint[1:]) / np.log(2)
print("(Middle point) The convergence rates are", convratemid)
print()

for i in range(8):
    quad = quadrature.Integrator(lambda x: np.sin(x), 0, np.pi/2, 10*pow(2, i))
    errtrapezoidal[i] = np.fabs(exacintegral - quad.trapezoidal())
    print(errtrapezoidal[i])

convratetrapezoidal = np.log(
    errtrapezoidal[:-1] / errtrapezoidal[1:]) / np.log(2)
print("(Trapezoidal) The convergence rates are", convratetrapezoidal)

for i in range(8):
    quad = quadrature.Integrator(lambda x: np.sin(x), 0, np.pi/2, 10*pow(2, i))
    errsimpson3_8[i] = np.fabs(exacintegral - quad.simpson3_8())
    print(errsimpson3_8[i])

convratesimpson3_8 = np.log(errsimpson3_8[:-1] / errsimpson3_8[1:]) / np.log(2)
print("(Simpson 3/8) The convergence rates are", convratesimpson3_8)

for i in range(8):
    quad = quadrature.Integrator(lambda x: np.sin(x), 0, np.pi/2, 10*pow(2, i))
    errsimpson1_3[i] = np.fabs(exacintegral - quad.simpson1_3())
    print(errsimpson1_3[i])

convratesimpson1_3 = np.log(errsimpson1_3[:-1] / errsimpson1_3[1:]) / np.log(2)
print("(Simpson 1/3) The convergence rates are", convratesimpson1_3)
