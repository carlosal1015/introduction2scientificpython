def non_blank_lines(thing):
    count = 0
    for line in thing:
        if line.strip():
            count += 1
    return count


print(non_blank_lines("Hola cómo estás"))
