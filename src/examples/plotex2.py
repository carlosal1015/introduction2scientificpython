import matplotlib.pyplot as plt
import numpy as np

# rea la figure
fig = plt.figure()

ax1 = fig.add_subplot(221)
ax2 = fig.add_subplot(222)
ax3 = fig.add_subplot(223)
ax4 = fig.add_subplot(224)

x = np.linspace(0, 2*np.pi, 40)
ax1.scatter(x, np.exp(x))
ax2.scatter(x, np.sqrt(2.*x))
ax3.scatter(x, np.sqrt(4.*x))
ax4.scatter(x, np.sin(8.*x))

# show the plot
plt.show()
