class Rectangle(object):
    def __init__(self, x0, y0, x1, y1):
        self.x0, self.y0, self.x1, self.y1 = x0, y0, x1, y1

    def __str__(self):
        return f"El rectángulo tiene vértices ({self.x0}, {self.y0}) y ({self.x1}, {self.y1})"

    def area(self):
        return (abs(self.x0 - self.x1)*abs(self.y0-self.y1))

    def contains(self, x, y):
        return (x <= abs(self.x0 - self.x1) and y <= abs(self.y0 - self.y1))


myRectangulo = Rectangle(0, 0, 5, 5)

otroRectangulo = Rectangle(0, 0, 3, 3)

print(myRectangulo)
print(otroRectangulo)

print(myRectangulo.area())
print(otroRectangulo.area())

print(myRectangulo.contains(x=2, y=5))
