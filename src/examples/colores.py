class Color(object):
    """Un color RGB tiene componentes rojo, verde y azul"""

    def __init__(self, r, g, b):
        self.red = r
        self.green = g
        self.blue = b

    def __str__(self):
        return f"({self.red}, {self.green}, {self.blue})"

    def __add__(self, other):
        return Color(self.red + other.red, self.green + other.green, self.blue + other.blue)

    def lightness(self):
        strongest = max(self.red, self.green, self.blue)
        weekless = min(self.red, self.green, self.blue)
        return 0.5 * (strongest + weekless) / 255


black = Color(128, 0, 128)
azul = Color(0, 0, 128)
print(f"Black es {black}")

print(f"La suma de black y azul es {black + azul}")

print(black.lightness())
print(dir(Color))
"""
dictionario = {"rojo":0, "verde": 0, "azul": 0}
print(dictionario.keys())
print(dictionario.values())
"""

print("Hello World")

print("We are in the Agrarian University.")