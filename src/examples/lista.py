whales = [5, 4, 7, 3, 2, 3, 2, 6, 4, 2, 1, 7, 1, 3]
del whales[-5]

print(whales)

x = []
print(type(x))

mylista = [{"x": 1, "y": 2}, [1, 2, 3, 4, 5], "UNALM", False]

print(sum(whales))

A = ['h', 'e', 'l', 'i', 'o']
B = ['a', 'z', 'u', 'f', 'r', 'e']

print(A + B)

colors = ['green', 'blue', 'red']

for color in colors:
    print(color, end=" ")

print(type(range))


phrase = """Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Dolor sed viverra ipsum nunc aliquet bibendum enim"""

list_phrase = phrase.split()

length = [len(list_phrase[i]) for i in range(len(list_phrase))]

for word, number in zip(list_phrase, length):
    print(f'La palabra "{word}" tiene {number} caracteres.')

colors = ['green', 'blue', 'red']

colors.append('black')
colors.reverse()
print(colors)
x = [1, 5, 2, 8]
x.sort()

print(x)

paises = [['China', 70000], ['Corea del Sur', 200]]

print(paises[1][1])
