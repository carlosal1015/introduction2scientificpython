import numpy as np


def funcionx(x):
    f = np.exp(x)*x
    return(f)


a, b = 0, 1
tramos = 10

h = (b-a)/tramos
x = a
# segmento: por cada dos tramos
suma = funcionx(x)
for i in range(0, tramos-2, 2):
    x = x + h
    suma = suma + 4*funcionx(x)
    x = x + h
    suma = suma + 2*funcionx(x)
# último segmento
x = x + h
suma = suma + 4*funcionx(x)
suma = suma + funcionx(b)
area = h*(suma/3)
# SALIDA
print('Integral: ', area)

"""
n = 1: regla del trapecio
\\int_{x_0}^{x_1}f(x)dx=h/2[f(x_0)+f(x_1)] -h^3/12f''(\xi) donde x_0<]xi<x_1

n = 2 regla de simpson
\\int_{x_0}^{x_1}f(x)dx=h/3[f(x_0)+4f(x_1)+f(x_2)]-h^5/90f^4(\xi)

n = 3 regla de tres octavos de Simpson

\\int_{x_0}^{x_1}f(x)dx=3h/8[f(x_0)+3f(x_1)+3f(x_2)+f(x_3)]-3h^5/80f^4(\xi)
"""