import quadrature
import numpy as np
"""
help(quadrature)

quad = quadrature.Integrator(np.exp, 0, 1, 10)

print('The inerval ({0},{1}) is divided into {2} '
      'subintervals. \nThe nodal points are {3} '.format(quad1.a, quad1.b, quad1.N, quad1.x))
"""

errmidpoint = np.zeros(8)
errtrapez = np.zeros(8)
errsimpson = np.zeros(8)
exactintegral = 1.

for i in range(8):
    quad = quadrature.Integrator(lambda x: np.exp(x)*x, 0, 1, 10*pow(2, i))
    errmidpoint[i] = np.fabs(exactintegral - quad.midpoint())
    print(errmidpoint[i])

convratemid = np.log(errmidpoint[:-1] / errmidpoint[1:]) / np.log(2)
print("(middle point) The convergence rates are", convratemid)

for i in range(8):
    quad = quadrature.Integrator(lambda x: np.exp(x)*x, 0, 1, 10*pow(2, i))
    errtrapez[i] = np.fabs(exactintegral - quad.trapez())
    print(errtrapez[i])

convratetrapez = np.log(errtrapez[:-1] / errtrapez[1:]) / np.log(2)
print("(Trapezoidal) The convergence rates are", convratetrapez)

for i in range(8):
    quad = quadrature.Integrator(lambda x: np.exp(x)*x, 0, 1, 10*pow(2, i))
    errsimpson[i] = np.fabs(exactintegral - quad.simpson())
    print(errsimpson[i])

convratesimpson = np.log(errsimpson[:-1] / errsimpson[1:]) / np.log(2)
print("(Simpson) The convergence rates are", convratesimpson)
