*********************************
Introduction to Scientific Python
*********************************

Schedule
--------

- `24.02: <resources/slides/L1.pdf>`_ Introduction to Python, syntaxis, variables, data types, operators, built-in mathematical functions.
- `25.02: <resources/slides/L2.pdf>`_ Functions, conditional statements, recursion, classes/objects, iterators, reading and writing files, visualization.
- `26.02: <resources/slides/L3.pdf>`_ Introduction to Numpy, numerical differenciation and integration.
- 27.02: Solving systems of linear equations.
- 28.02: Example: a finite difference code for Poisson equation in 2D.
