# SOME DESCRIPTIVE TITLE.
# Copyright (C)
# This file is distributed under the same license as the Introduction to
# Scientific Python package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Introduction to Scientific Python \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-08 19:16-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../../source/theory/lesson3/numpy.rst:3 4bd7d51c013645ea87276dc19b426d9e
msgid "NumPy |:abacus:|"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:7 60f4c0405f9945aa8f2edc9b736611b4
msgid ":mod:`numpy`"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:11 375fe5a7f72b422bb92d3009b0374aef
msgid ":class:`array`"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst f9782639c7db4c828fd5d732d80ccde3
msgid "object"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:15 85a5a4a28f404e3d883f3a7f61a26f74
msgid "array_like"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst 00a3bebc8bf74dd58bb6cebfe6d8de29
msgid "pdtype"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:17 cfdd3d4ef6b04654b64444aebfbf4ca4
msgid "data-type, optional"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst 51aebcd0c35b455bae10a23b66330f2e
msgid "copy"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:18
#: ../../source/theory/lesson3/numpy.rst:20 794855cfd62249b79d93b353f90bae41
#: 8f7ff65e77ea4ed9a7da7bb2324f65bb
msgid "bool, optional"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst d2d8795043ae4ca9910bea5aaebfbe33
msgid "order"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:19 a1262de63ba54a07aff073fc50f6bb88
msgid "{'K', 'A', 'C', 'F'}, optional"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst 8e717f253cfb407abf38185fb412262b
msgid "subok"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst f64a89aa638c47738ab9110815aa9b47
msgid "ndmin"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:21 0849594c857540458e3d37aa9a18d500
msgid "int, optional"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:24 44af95c6c9b84090b88f3366156bd132
msgid "Create an array."
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:28 479863c25d854a3fa3acd8eb40e5fabe
msgid "The parrot module"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:34 b68adb18c5544ec49b31d4dc9aa15655
msgid "The parrot module is a module about parrots."
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:36 51c663dfdb6b4c4787e212b1284d232e
msgid "Doctest example:"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:43 80f93765ecb642c2997e3eaadf4159ef
msgid "Test-Output example:"
msgstr ""

#: ../../source/theory/lesson3/numpy.rst:49 e15e6ffa65924b35a2978f66e18fd26e
msgid "This would output:"
msgstr ""

#~ msgid "Examples"
#~ msgstr ""

#~ msgid "Upcasting:"
#~ msgstr ""

#~ msgid "More than one dimension:"
#~ msgstr ""

#~ msgid "Minimum dimensions 2:"
#~ msgstr ""

#~ msgid "Type provided:"
#~ msgstr ""

#~ msgid "Data-type consisting of more than one element:"
#~ msgstr ""

#~ msgid "Creating an array from sub-classes:"
#~ msgstr ""

#~ msgid "Todo"
#~ msgstr ""

#~ msgid "blah blah"
#~ msgstr ""

#~ msgid "A Plain Title"
#~ msgstr ""

#~ msgid "This is the text of the section."
#~ msgstr ""

#~ msgid "It refers to the section title, see :ref:`A Plain Title`."
#~ msgstr ""

#~ msgid "Since Pythagoras, we know that :math:`a^2 + b^2 = c^2`."
#~ msgstr ""

