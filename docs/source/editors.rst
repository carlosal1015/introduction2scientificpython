********************************************************
Python's text editors and IDE |:man_technologist_tone1:|
********************************************************

Loosely speaking, you have two possibilities. The first one is work
with some *text editors* and join with some Python plugin like REPL
(Read–eval–print loop) feature, this program can help you for writing
scripts, edit the code or text. The second one is work with a Python's
IDE (Integrated development environment), which have an debugger, autocomplete,
linting function, libraries, terminal incorporated, control version services,
posibility to create a project (folder).

.. seealso::

    Please check the full list for text editors.

    * https://wiki.archlinux.org/index.php/List_of_applications/Documents#Text_editors
    * https://wiki.archlinux.org/index.php/List_of_applications/Utilities#Python_IDEs


In anywhere installation of Python, the program called
`IDLE <https://docs.python.org/3/library/idle.html>`_ is installed.
For run this program in the background, please type the following
instruction:

.. code:: console

    [user@hostname ~]$ idle &

.. figure:: _static/img/idle.png
    :align: center
    :alt: IDLE.

.. tabs::

  .. tab:: |vim-program|

    .. code:: console

        [user@hostname ~]$ sudo dnf install vim     # Fedora, RHEL, Clear Linux.
        [user@hostname ~]$ sudo apt install vim     # Debian, MX, deepin, elementary, Raspbian.
        [user@hostname ~]$ sudo pacman -Sy vim      # Arch Linux, Manjaro.
        [user@hostname ~]$ sudo zypper install vim  # OpenSUSE (Tumbleweed, Leap), SUSE.

    After install, for example in OpenSUSE Tumbleweed:

    .. figure:: _static/img/zypper-vim.png
        :align: center
        :alt: Installing Vim on OpenSUSE.

    Now, you already have installed vim in your system, type |vim| for execute.

    .. figure:: _static/img/vim.png
        :align: center
        :alt: Vi IMproved.

    .. tabs::

      .. tab:: |spacevim|

        .. code:: console

            [user@hostname ~]$ curl -sLf https://spacevim.org/install.sh | bash
            [user@hostname ~]$ vim

        .. figure:: _static/img/spacevim.png
            :align: center
            :alt: SpaceVim.

        `Use Vim as a Python IDE <https://spacevim.org/use-vim-as-a-python-ide>`_.

        .. figure:: _static/img/spacevim-1.png
            :align: center
            :alt: SpaceVim-1.

        .. figure:: _static/img/spacevim-2.png
            :align: center
            :alt: SpaceVim-2.

        .. figure:: _static/img/spacevim-repl.png
            :align: center
            :alt: SpaceVim-repl.

  .. tab:: |emacs-program|

    Some people think that the editor created by
    `Richard Stallman <https://en.wikipedia.org/wiki/Richard_Stallman>`_
    in the 80's uses cryptic keyboard shortcuts, in fact, this
    powerful Lisp machine allows you to read emails, surf the
    Internet, open images and videos.

    .. figure:: _static/img/learn-emacs.png
      :align: center
      :alt: ABC.

    .. todo::

      Show this site in emacs.

    You can use ``emacs-election`` $\in\{$ |emacs|, ``emacs-nox``, ``emacs-lucid`` $\}$.

    .. code:: console

        [user@hostname ~]$ sudo dnf install emacs-election     # Fedora, RHEL, Clear Linux.
        [user@hostname ~]$ sudo apt install emacs-election     # Debian, MX, deepin, elementary, Raspbian.
        [user@hostname ~]$ sudo pacman -Sy emacs-election      # Arch Linux, Manjaro.
        [user@hostname ~]$ sudo zypper install emacs-election  # OpenSUSE (Tumbleweed, Leap), SUSE.

    .. note::

        If you are macOS user, could you opt for the program `Aquamacs <http://aquamacs.org>`_.

        .. code-block:: console

            Users-name:~ User$ brew cask install aquamacs

    .. figure:: _static/img/emacs-nox.png
        :align: center
        :alt: Emacs.

    .. tip::

        Emacs have many options like ``--no-window-system``, ``-nw``.

        .. code:: console

            [user@hostname ~]$ emacs -nw

    ----------------------------
    Emacs Key Binding Philosophy
    ----------------------------

    The three core concepts for Emacs key bindings are *modifiers*,
    *chords* and *sequences*.

    * A *modifier* key is any one of the following. Anything else is a "non-modifer key".
    * A *chord* is made up of zero or more modifier keys pressed at the same time as a single non-modifier key.
    * A *sequence* is a series of chords, pressed and released in a row. The most important command in Emacs, :kbd:`Ctrl-x` + :kbd:`Ctrl-c`, is a great example of a sequence.

    M-x quickrun (show the output)
    The text you are editing in Emacs resides in an object called a buffer.
    SPC Tab: switch to previous buffer.
    SPC-x

    .. table::
         :align: center

         +------------------+-----------------------+-----------+----------------+
         | Description      | Emacs                 | Vim       | Space          |
         +==================+=======================+===========+================+
         | Open file        | :kbd:`C-x` :kbd:`C-f` | :kbd:`:e` | :kbd:`SPC-f-f` |
         +------------------+-----------------------+-----------+----------------+
         | Exit             | :kbd:`C-x` :kbd:`C-c` | :kbd:`:x` | :kbd:`SPC-q-q` |
         +------------------+-----------------------+-----------+----------------+
         | Help             | :kbd:`C-x` :kbd:`C-c` | :kbd:`:x` | :kbd:`SPC-q-q` |
         +------------------+-----------------------+-----------+----------------+
         | Copy             | :kbd:`C-` :kbd:`C-` | :kbd:`:` | :kbd:`SPC-` |
         +------------------+-----------------------+-----------+----------------+
         | Paste            | :kbd:`C-` :kbd:`C-` | :kbd:`:` | :kbd:`SPC-` |
         +------------------+-----------------------+-----------+----------------+
         | Undo            | :kbd:`C-` :kbd:`C-` | :kbd:`:` | :kbd:`SPC-` |
         +------------------+-----------------------+-----------+----------------+
         | Redo            | :kbd:`C-` :kbd:`C-` | :kbd:`:` | :kbd:`SPC-` |
         +------------------+-----------------------+-----------+----------------+
         | Kill Buffer      |                       |           | :kbd:`SPC-b-k` |
         +------------------+-----------------------+-----------+----------------+
         | Write file       | :kbd:`C-x` :kbd:`C-s` | :kbd:`:w` | :kbd:`SPC-f-s` |
         +------------------+-----------------------+-----------+----------------+
         | Horizontal split |                       |           | :kbd:`SPC-w-s` |
         +------------------+-----------------------+-----------+----------------+
         | Vertical split   |                       |           | :kbd:`SPC-w-v` |
         +------------------+-----------------------+-----------+----------------+
         | New empty buffer |                       |           | :kbd:`SPC-b-N` |
         +------------------+-----------------------+-----------+----------------+
         |                  | :kbd:`M-x`            |           |                |
         +------------------+-----------------------+-----------+----------------+

    .. tabs::

      .. tab:: |spacemacs|

        .. code:: console

            [user@hostname ~]$ git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d

        `Python layer <https://www.spacemacs.org/layers/+lang/python/README.html>`_.

        .. figure:: _static/img/spacemacs.png
            :align: center
            :alt: Spacemacs.

        .. figure:: _static/img/spacemacs-dotfile.png
            :align: center
            :alt: Spacemacs dotfile.

      .. tab:: |doom-emacs|

        .. code:: console

            [user@hostname ~]$ git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
            [user@hostname ~]$ export PATH=$HOME/.emacs.d/bin:$PATH >> ~/.zshrc
            [user@hostname ~]$ source ~/.zshrc
            [user@hostname ~]$ doom install

        .. figure:: _static/img/doom-emacs-home.png
            :align: center
            :alt: Doom Emacs home.

        .. figure:: _static/img/doom-emacs-test.png
            :align: center
            :alt: Doom Emacs Test.

  .. tab:: |vscodium|

      We prefer `VSCodium <https://vscodium.com>`_ over VS Code. The steps
      for the mainstreams linux distributions are in the website.

      .. code-block:: console

          Users-name:~ User$ brew cask install vscodium

      .. code-block:: console

          [user@hostname ~]$ yay -Sy vscodium

      .. figure:: _static/img/vscodium.png
          :align: center
          :alt: ABC.

      Please check out `the tutorial <https://code.visualstudio.com/docs/python/python-tutorial>`_.

  .. tab:: |pycharm|

      First install `PyCharm Community edition <https://www.jetbrains.com/de-de/pycharm/download/#section=linux>`_,
      open the terminal emulator and type

      .. code-block:: console

          [user@hostname ~]$ sudo snap install pycharm-community --classic # Tested on lab with Ubuntu 18.04 LTS.
          [user@hostname ~]$ sudo apt install python3-distutils # See https://askubuntu.com/q/1184913/791670

      After the installation, please run the program PyCharm, this is found
      in the launcher application.

      .. figure:: _static/img/pycharm-welcome.png
          :align: center
          :alt: ABC.

      If you want to create a new project choose this and select Pure Python
      on the left.

      .. figure:: _static/img/pycharm-newproject.png
          :align: center
          :alt: ABC.

      Otherwise, if you want to open the folder where your Python files are,
      choose to open. Find the path of your working folder and click OK.

      .. figure:: _static/img/pycharm-choose.png
          :align: center
          :alt: ABC.

      Now, wait a few seconds until you finish indexing the packages and see
      in the image below that you need to add the configuration.

      .. figure:: _static/img/pycharm-project.png
          :align: center
          :alt: ABC.

      Please go to ``File/Settings`` as seen in the image or press :kbd:`Ctrl` + :kbd:`Alt` + :kbd:`S`.

      .. figure:: _static/img/pycharm-settings.png
          :align: center
          :alt: ABC.

      Now, click on Project Interpreter and select the appropriate one,
      you will see the list of contained packages.

      .. figure:: _static/img/pycharm-interpreter.png
          :align: center
          :alt: ABCD.

      Finally, in each new file you want to run, you must perform the action
      Run or press :kbd:`Alt` + :kbd:`Shift` + :kbd:`F10`.

      .. figure:: _static/img/pycharm-run.png
          :align: center
          :alt: ABCDE

      Please check out `this guide <https://realpython.com/pycharm-guide>`_
      or `other guide <https://www.jetbrains.com/help/pycharm/creating-and-running-your-first-python-project.html>`_.

.. |vim-program| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/vim.svg"><br/>
  </div>
  <div style="display:inline-block;">
    <h3 style="width:auto;">
      Vim
    </h3>
  </div>

.. |vim| raw:: html

   <tt><a href="https://www.vim.org">vim</a></tt>

.. |spacevim| raw:: html

  <div style="display:inline-block;">
    <a target="_blank" rel="noopener noreferrer" href="https://spacevim.org">
      <img style="width:200px;" src="_static/img/spacevim-logo.png">
    </a>
  </div>

.. |emacs-program| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/emacs.svg"><br/>
  </div>
  <div style="display:inline-block;">
    <h3 style="width:auto;">
      GNU Emacs
    </h3>
  </div>

.. |emacs| raw:: html

   <tt><a href="https://www.gnu.org/software/emacs">emacs</a></tt>

.. |spacemacs| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/spacemacs-logo.svg">
  </div>
  <div style="display:inline-block;">
    <h4 style="width:auto;">
      <a target="_blank" rel="noopener noreferrer" href="https://www.spacemacs.org">
      (spacemacs)</a>
    </h4>
  </div>

.. |doom-emacs| raw:: html

  <div style="display:inline-block;">
    <h4 style="width:auto;">
      <a target="_blank" rel="noopener noreferrer" href="https://github.com/hlissner/doom-emacs">
      Doom Emacs</a>
    </h4>
  </div>

.. |vscodium| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/code.png"><br/>
  </div>
  <div style="display:inline-block;">
    <h3 style="width:auto;">
      VSCodium
    </h3>
  </div>

.. |pycharm| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/pycharm.svg"><br/>
  </div>
  <div style="display:inline-block;">
    <h3 style="width:auto;">
      <a target="_blank" rel="noopener noreferrer" href="https://www.jetbrains.com/pycharm">
      PyCharm</a>
    </h3>
  </div>

`Atom <https://atom.io>`_. More interesting information in
`this survey <https://insights.stackoverflow.com/survey/2019#technology-_-most-popular-development-environments>`_.
