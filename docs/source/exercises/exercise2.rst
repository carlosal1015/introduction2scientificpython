.. _exercise_two:

Exercise sheet $2$
------------------

The aim of this exercise sheet is to provide guidance for implementing
several quadrature formulas for numerical integration. We are
interested in approximating

.. math::
    :label: indefined integral

    \int_{a}^{b}f(x)\,\mathrm{d}x

where $a,b\in\mathbb{R}$.

For a given $N\in\mathbb{N}_{>0}$, $h=\frac{b-a}{N}$ be the width of
subintervals. We call $x_{i}=a+hi$ the **nodal points**.

1. Open an empty file and name it as quadrature.py

.. code-block:: python
   :linenos:
   :emphasize-lines: 1,12

   import numpy as np

   class Integrator():
       """A class to integrate functions numerically.
       Create an object by Integrator(f, a, b, N)
       f = Integrand function
       a = Interval lowerbound
       b = Interval upperbound
       N = Number of subintervals
       """

       def __init__(self, f, a, b, N):
           """(Integrator, f, a, b, N) -> NoneType
           Cretate an Integrator with N intervals.
           """
           self.a = a
           self.b = b
           self.N = N
           self.x = 0   # TODO: Part (a)
           self.h = 0   # TODO: Part (b)
           self.f = f   # TODO: Part (c)


>>> import quadrature
>>> help(quadrature)
Help on module quadrature:
NAME
    quadrature
CLASSES
    builtins.object
        Integrator
    class Integrator(builtins.object)
     |  Integrator(f, a, b, N)
     |  
     |  A class to integrate functions numerically.
     |  Create an object by Integrator(f, a, b, N)
     |  f Integrand function
     |  a Interval lowerbound
     |  b Interval uperbound
     |  N Number of subintervals
     |  
     |  Methods defined here:
     |  
     |  __init__(self, f, a, b, N)
     |      (Integrator, f, a, b, N) -> NoneType
     |      Create an Integrator with N intervals
     |  
     |  midpoint(self)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
FILE
    /home/carlosal1015/Git_Projects/Python/Scientific_Python/source/exercises/quadrature.py
>>> quad = quadrature.Integrator(0, 0, 1, 10)
>>> print(f'The interval ({quad.a}, {quad.b}) is \
divided into {quad.N} subintervals.\nThe nodal \
points are {quad.x}.')
The interval (0, 1) is divided into 10 subintervals.
The nodal points are [0.  0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1. ].

.. math::
    :label: midpoint

    M_{h}\left(f\right) = h\sum_{i=1}^{N}f\left(\frac{x_{i}+x_{i-1}}{2}\right).


.. code-block:: python
   :linenos:
   :emphasize-lines: 1,12

   import numpy as np

   class Integrator():
       """
       The same code as above.
       """

       def __init__(self, f, a, b, N):
           """
           The same code as above.
           """
        
        def midpoint(self):
           """(Integrator) -> float
           Numerical integration by midpoint rule
           """
           s = # TODO: Part (a)
           y = # TODO: Part (b)
           T = # TODO: Part (c)
           return T

>>> import quadrature
>>> import numpy as np
>>> quad = quadrature.Integrator(np.exp, 0, 1, 10)
>>> result = quad.midpoint()
>>> print(result)

>>> exactintegral = np.exp(1)-1
>>> quad = quadrature.Integrator(np.exp, 0, 1, 10)
>>> errmidpoint[0] = np.fbs(exactintegral - quad.midpoint())
>>> print(result)

>>> convratemid = np.log(errmid[:-1] / errmid[1:]) / np.log(2)
>>> print(convratemid)

.. math::
    :label: xexp(x)

    \begin{aligned}
    \int\limits_{0}^{1}x\exp(x)\mathrm{d}x
    &={x\cdot\left.\exp\left(x\right)\right|}^{1}_{0}-\int_{0}^{1}\exp\left(x\right)\\
    &=\left[1\cdot\exp\left(1\right)-0\cdot\exp\left(0\right)\right]-{\left.\exp\left(x\right)\right|}^{1}_{0}\\
    &=\left[\exp\left(1\right)\right]-\left[\exp\left(1\right)-\exp\left(0\right)\right]\\
    \int\limits_{0}^{1}x\exp(x)\mathrm{d}x
    &=1.
    \end{aligned}