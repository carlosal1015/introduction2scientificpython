.. _exercises:

*********
Exercises
*********

.. toctree::
    :titlesonly:
    :maxdepth: 1

    exercise1.rst
    exercise2.rst