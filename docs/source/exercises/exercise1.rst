.. _exercise_one:

.. role:: python(code)
   :language: python

===================
Exercise sheet $1$:
===================

(i). Predict the output of following expressions.

(ii). Type them on Python shell and compare with your predictions.

========================  ===============  ==============  =========================
Expresión                 Predicción       Salida          Tipo
========================  ===============  ==============  =========================
(a) :python:`7-2`         :python:`5`      :python:`5`     :python:`<class int>`
(b) :python:`4*1.5`       :python:`6`      :python:`6`     :python:`<class float>`
(c) :python:`6/3`         :python:`2.0`    :python:`2.0`   :python:`<class float>`
(d) :python:`6/-3`        :python:`-2.0`   :python:`-2.0`  :python:`<class float>`
(e) :python:`6//-3`       :python:`-2`     :python:`-2`    :python:`<class int>`
(f) :python:`6%3`         :python:`2`      :python:`2`     :python:`<class int>`
(g) :python:`6.0%3`       :python:`2.0`    :python:`2.0`   :python:`<class float>`
(h) :python:`6%3.0`       :python:`2.0`    :python:`2.0`   :python:`<class float>`
(i) :python:`-6%3`        :python:`-2`     :python:`-2`    :python:`<class int>`
(j) :python:`6/-3.0`      :python:`-2.0`   :python:`-2.0`  :python:`<class float>`
(k) :python:`2+4*3`       :python:`14`     :python:`14`    :python:`<class int>`
(l) :python:`(2+4)*3`     :python:`18`     :python:`18`    :python:`<class float>`
(m) :python:`'a'+'b'`     :python:`'ab'`   :python:`'ab'`  :python:`<class str>`
(n) :python:`'a'+'3'`     :python:`'a3'`   :python:`'a3'`  :python:`<class str>`
(o) :python:`0 or 1`      :python:`True`   :python:`1`     :python:`<class int>`
(p) :python:`0 and 1`     :python:`False`  :python:`0`     :python:`<class int>`
(q) :python:`2 or not 2`  :python:`True`   :python:`2`     :python:`<class int>`
========================  ===============  ==============  =========================

In an assignment statement, python evaluates the right-hand side first,
before doing the actual setting of variables. Before typing following
lines on shell, write your prediction in the dotted areas.

.. tabs::

  .. tab:: |numpy|

    NumPy is the fundamental package for scientific computing with
    Python. It contains among other things:

    - A powerful N-dimensional array object.
    - Sophisticated (broadcasting) functions.
    - Tools for integrating C/C++ and Fortran code.
    - Useful linear algebra, Fourier transform, and random number capabilities.

  .. tab:: |sympy|

    SymPy is a Python library for symbolic mathematics. It aims to
    become a full-featured computer algebra system (CAS) while keeping
    the code as simple as possible in order to be comprehensible and
    easily extensible.

  .. tab:: |matplotlib|
    
    Matplotlib is a Python 2D plotting library which produces
    publication quality figures in a variety of hardcopy formats and
    interactive environments across platforms.

  .. tab:: |pandas|

    pandas is a fast, powerful, flexible and easy to use open source
    data analysis and manipulation tool, built on top of the Python
    programming language.

.. |numpy| raw:: html

  <div style="display:inline-block;">
    <a target="_blank" rel="noopener noreferrer" href="https://numpy.org">
      <img style="width:150px;" src="../_static/img/numpy.png">
    </a>
  </div>

.. |sympy| raw:: html

  <div style="display:inline-block;">
    <img style="width:100px;" src="../_static/img/sympy.png"><br/>
  </div>
  <div style="display:inline-block;">
    <h4 style="width:auto;">
      <a target="_blank" rel="noopener noreferrer" href="https://www.sympy.org">
      SymPy</a>
    </h4>
  </div>

.. |matplotlib| raw:: html

  <div style="display:inline-block;">
    <a target="_blank" rel="noopener noreferrer" href="https://matplotlib.org">
      <img style="width:150px;" src="../_static/img/matplotlib.svg">
    </a>
  </div>

.. |pandas| raw:: html

  <div style="display:inline-block;">
    <a target="_blank" rel="noopener noreferrer" href="https://pandas.pydata.org">
      <img style="width:150px;" src="../_static/img/pandas.svg">
    </a>
  </div>

.. code-block:: console
   :caption: Exercise1_1.py
   :name: Exercise1_1

   a)	5	<class 'int'>
   b)	6.0	<class 'float'>
   c)	2.0	<class 'float'>
   d)	-2.0	<class 'float'>
   e)	-2	<class 'int'>
   f)	0	<class 'int'>
   g)	0.0	<class 'float'>
   h)	0.0	<class 'float'>
   i)	0	<class 'int'>
   j)	-2.0	<class 'float'>
   k)	14	<class 'int'>
   l)	18	<class 'int'>
   m)	ab	<class 'str'>
   n)	a3	<class 'str'>
   o)	1	<class 'int'>
   p)	0	<class 'int'>
   q)	2	<class 'int'>

.. code-block:: python
   :caption: Exercise1_2_a.py
   :name: Exercise1_2_a
   :linenos:

   >>> x, y = 1, 2
   >>> x = y
   >>> y = x + y
 
.. code-block:: python
   :caption: Exercise1_2_b.py
   :name: Exercise1_2_b

   >>> x, y = 1, 2
   >>> x, y = y, x + y
   >>> x
   2
   >>> y
   3

.. code-block:: python
   :caption: Exercise1_3.py
   :name: Exercise1_3

   >>> first = 'world'
   >>> last = 'hello'
   >>> print(last + ', ' + first)
   hello, world
   >>> print(last, ',', first)
   hello , world