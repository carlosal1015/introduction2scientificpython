.. Introduction to Scientific Python documentation master file, created by
   sphinx-quickstart on Wed Feb 26 15:54:23 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Introduction to Scientific Computing with Python |:circus_tent:|
================================================================

|prompt|

.. image:: _static/img/python.png
    :class: align-right

.. toctree::
    :titlesonly:
    :caption: Content
    :maxdepth: 4

    Installation <installation>
    theory/index
    exercises/index
    references

.. toctree::
   :caption: Appendix
   :maxdepth: 1

   Editors <editors>
   Changelog <changes>
   genindex
   about/index

.. note::

    The name of the Python language comes from the BBC show
    `Monty Python's Flying Circus <https://www.bbc.com/historyofthebbc/anniversaries/october/monty-pythons-flying-circus>`_.

.. |prompt| raw:: html

   <script type="text/x-thebe-config">
      {
         bootstrap: true,
         binderOptions: {
         repo: "carlosal1015/introduction2scientificpython",
         repoProvider: "gitlab",
         },
         kernelOptions: {
         name: "python3",
         },
      }
   </script>
   <script type="text/javascript" src="https://unpkg.com/thebelab@latest"></script>

   <pre data-executable="true" data-language="python">from numpy import __version__
   print(__version__)
   </pre>
