=============================
Release History |:clipboard:|
=============================

v0.6.0
------

:Date: Mar 29, 2020

New Features
-------------

* Support to YouTube videos.

v0.5.0
------

:Date: Mar 21, 2020

* Render math expressions with $\KaTeX$.
* Copyright disabled.

v0.4.0
------

:Date: Mar 14, 2020

* Add link to slides.

v0.3.0
------

:Date: Mar 13, 2020

* Translation to Spanish language.

v0.2.0
------

:Date: Mar 2, 2020

* Docker image with Python 3.8.2, |numpy|, |matplotlib|, |notebook|, |pandas|.

.. |numpy| raw:: html

   <tt><a href="https://numpy.org">numpy</a></tt>

.. |matplotlib| raw:: html

   <tt><a href="https://matplotlib.org">matplotlib</a></tt>

.. |notebook| raw:: html

   <tt><a href="https://jupyter.org">notebook</a></tt>

.. |pandas| raw:: html

   <tt><a href="https://pandas.pydata.org">pandas</a></tt>

v0.1.0-alpha
------------

:Date: Feb 26, 2020

* Initial release.
* Start keeping changelog |:smile:|.
