=============
Interpolación
=============

¿Puede encontrar una función cuya gráfica pase a través de cada uno
de los puntos? Esto es, puede encontrar una función $p$ tal que
$p(x_i)=y_i,\forall 0\leq i\leq n$.

El proceso de encontrar y evaluar tal función es lo que llamamos
interpolación, $x_0,x_1,\ldots,x_n$ son llamados nodos. El proceso
de encontrar un polinomio que pase a través de un conjunto de
puntos datos es llamado interpolación polinómica.

Interpolación lineal
====================

Dados dos puntos $(x_0, y_0)$, $(x_1, y_1)$, necesitamos encontrar
una función lineal $p(x)=a_1x+a_0$ que pase a través de esos puntos.
Esto significa, $p(x_0)=y_0$ y $p(x_1)=y_1$.

Dado que $p(x)$ es, en este caso, una función linel, y dado que nos
dan dos puntos $(x_0,y_0)$ y $(x_1,y_1)$, el polinomio $p(x)$ es la
única recta que pasa a través de estos puntos dados. Por lo tanto,
$a_1=\frac{y_1-y_0}{x_1-x_0}$ es la pendiente de la recta, y
$a_0=y_1-a_1x_1$. Además

.. math::

  \begin{aligned}
  p(x)
  &=a_1x+a_0\\
  &=\frac{y_1-y_0}{x_1-x_0}x+y_1-\frac{y_1-y_0}{x_1-x_0}x_1\\
  &=\left(\frac{x-x_1}{x_0-x_1}\right)y_0+\left(\frac{x-x_0}{x_1-x_0}\right)y_1\\
  \end{aligned}

Ejemplo

Una **proposición** es una oración declarativa que es verdadero o
falso, pero no ambos.

Si $p(x)$ es cualquier proposición acerca de la variable $x$, entonces
$\{x:p(x)\}$ denota el conjunto de todos los valores de $x$ para cual
$p(x)$ es verdadero. A veces es llamado el "conjunto verdad" de $p(x)$.

Una **función proposicional** (o **predicado**) es una oración
declarativa que contiene uno o más variables, que se convierte en una
proposición cuando las variables son reemplazadas por constantes.

Cuantificador universal. Suponga una función proposicional $P(x)$ es
verdadera para todos los valores de $x$ en su dominio. Ese hecho es
en sí mismo una proposición, que denotamos

.. math::
  \forall x, P(x)

Cuantificador existencial. Suponga una función proposicional $P(x)$
es verdadera para algún (por lo menos uno) valor de $x$ en su dominio.
Ese hecho es en sí mismo una proposición, que denotamos

.. math::

  \exists x\ni P(x)

Las declaraciones cuantificadas ocurren frecuentemente en matemáticas
que es importante para poder formar sus negaciones correctamente. El
procedimiento puede ser visto complicado al principio, pero la idea es
muy simple, y con un poco de práctica te volverás bastante bueno en eso.
El siguiente principio es extremadamente importante.

* $\mathord{\sim}\left(\forall x,P(x)\right)\equiv\exists x\ni\mathord{\sim}P(x)$.
* $\mathord{\sim}\exists x\ni P(x)\equiv\forall x,\mathord{\sim}P(x)$.

El **conjunto vacío**, $\emptyset$ es el conjunto que no tiene miembros.
Así, por ejemplo, $\emptyset=\{x:x\neq x\}$.

En Python, las funciones son objetos (valores) y se manejan como otros
objetos. Por lo tanto, puede pasar una función como argumento en una
llamada a otra función. Del mismo modo, una función puede devolver otra
función como resultado de una llamada. Una función, como cualquier otro
objeto, puede vincularse a una variable, un elemento en un contenedor o
un atributo de un objeto. Las funciones también pueden ser claves en un
diccionario. Por ejemplo, si necesita encontrar rápidamente el inverso
de una función dada la función, puede definir un diccionario cuyas claves
y valores sean funciones y luego hacer que el diccionario sea bidireccional
(usando algunas funciones del módulo ``math``.

La declaración ``def`` define algunos atributos de una función objeto.

>>> temps = dict(Oslo=13, London=15.4, Paris=17.5)


Considere el polinomio $p(x)=-1+x^2+3x^7$.

El dato asociado con este polinomio puede ser visto como un conjunto
de pares potencia-coeficiente, en este caso $-1$ pertenece a la
potencia $0$, el coeficiente $1$ pertenece a la potencia $2$, y el
coeficiente $3$ pertenece a la potencia $7$. Un diccionario puede
ser usado para mapear el la potencia al coeficiente:

>>> p = dict(0=1, 2=1, 7=3)