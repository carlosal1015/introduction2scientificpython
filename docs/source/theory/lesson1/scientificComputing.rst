=============================
What is scientific computing?
=============================

The scientific computing is the collection of tools, techniques, and
theories required to solve on a computer mathematical models of
problems in science and engineering.

.. image:: ../../_static/img/Drawing-2.png
  :width: 500 px
  :alt: Alternative text.
  :align: center

The mathematical models of all of these problems are systems of
differential equations, either ordinary or partial. Differential
equations come in all "sizes and shapes," and even with the largest
computers we are nowhere near being able to solve many of the problems
posed by scientists and engineers.

.. image:: ../../_static/img/Drawing-3.png
  :width: 500 px
  :alt: Alternative text
  :align: center

In summary, then, scientific computing draws on mathematics and
computer science to develop the best ways to use computer systems
to solve problems from science and engineering. This relationship
is depicted schematically in Figure 1.1.1.

.. image:: ../../_static/img/Drawing-4.png
  :width: 500 px
  :alt: Alternative text
  :align: center

.. image:: ../../_static/img/Drawing-5.png
  :width: 500 px
  :alt: Alternative text
  :align: center

.. image:: ../../_static/img/Drawing-6.png
  :width: 500 px
  :alt: Alternative text
  :align: center

.. figure:: ../../_static/img/mathematicalmodelling.svg
  :figwidth: 500 px
  :alt: Alternative text
  :align: center

  This is a caption for a figure. Text should wrap around the caption.

Scientific computing covers the collection of tools, techniques and
theories required to solver on a computer mathematical models of
problems in science and engineering.