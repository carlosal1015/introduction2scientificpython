===============
What is Python?
===============

Python is an interpreted, high-level, general-purpose programming
language. Released in $1991$, Guido van Rossum is the autor of the
Python programming language.

The Python interpreter is usually located at :file:`/usr/bin/python`,
this is a link which points to the file ``python -> python3 -> python3.8``.
On Windows systems, it is installed in the Program Files directory.
Adding this directory in the environment variable ``PATH`` makes it
possible to execute it by typing in the command prompt the command
``python``.

.. figure:: ../../_static/img/python-pactree.png
  :figwidth: 750 px
  :alt: Alternative text
  :align: center

  The tree diagram for ...

.. figure:: ../../_static/img/python-numpy-pactree.png
  :figwidth: 750 px
  :alt: Alternative text
  :align: center

  The tree diagram for ...

.. figure:: ../../_static/img/python-matplotlib-pactree.png
  :figwidth: 750 px
  :alt: Alternative text
  :align: center

  The tree diagram for ...

The full list of command options are in :manpage:`python` manual pages.

History of Python
-----------------

.. figure:: ../../_static/img/guido.jpg
  :figwidth: 300 px
  :alt: Alternative text
  :align: right

  `Guido van Rossum <https://gvanrossum.github.io>`_ was born on $1956$ in The Netherlands.

Se graduó en licenciatura en matemática en University of Amsterdam con la tesis

In October 2019 he retired.

The first prgramas
Encontrar enlaces simbólicos a una ruta dada
convertir un enlace simbólico a un directorio en un directorio real
Crear un archivo de etiquetas para programas Python
muestra diferentes sufijos entre argumentos
comprueba los archivos para los que rcsdiff devuelve un estado de salida distinto de cero

* 1989  0.9: Stichting Mathematisch Centrum, Amsterdam, The Netherlands
* January 1991 1.0: ``lambda``, ``map``, ``filter`` and ``reduce``.
* January 1991 1.2:
* January 1991 1.4: Keyword arguments
* October 2000 2.0
* December 2008 3.0
* February 2020 3.8.2
* ?? 2021 4.0

Yahoo March 1999

¿Para qué se usa?

creación rápida de prototipos

raspado web

tirar, programación ad hoc

dirección de aplicaciones científicas

lenguaje de extensión

aplicaciones de bases de datos

Aplicaciones GUI

¿Quién lo está usando?

LLNL, Fermilab (dirección)
Proyecto Alice en CMU (gráficos 3D)
Dominio de objeto (herramienta UML extendida)
InfoSeek (lenguaje de extensión, scripting)
Industrial Light & Magic (todo)
Yahoo! (CGI en YahooMail)
Creaciones digitales (sitio web de Zope mgt)
RedHat (herramientas de instalación de Linux)

Comparado con Perl

Más fácil de aprender.
--especialmente para usuarios poco frecuentes
Código más legible
- mantenimiento de código mejorado
Menos efectos secundarios "mágicos"
Más garantías de "seguridad"
Mejor integración de Java
Algunas cosas son más lentas

En comparación con Java

Código 5-10 veces más conciso
Escritura dinámica
Desarrollo mucho más rápido
- sin fase de compilación
- menos mecanografía

Si, corre mas lento
- pero el desarrollo es mucho más rápido
Lo mismo (pero más) para C / C ++

.. code:: python

   def gcd(a, b):
      """Greatest common divisor of two integers
      """
      while b!=0:
         a, b = b, a%b  # parallel assignment
      return a

.. note::

   Sin declaraciones

   Sangría + dos puntos para agrupación de instrucciones

   Parte de la cadena de documentación de la sintaxis de la función

   Asignación paralela (para intercambiar ayb: "a, b = b, a")

.. code:: python

   class Stack:
      """A well-known data structure # doc string
      """
      def __init__(self): # constructor
         self.items[]
      def push(self, x):
         self.items.append(x) # the sky is the limit
      def pop(self):
         x = self.items[-1] # what happens if it's empty?
         del self.items[-1]
         return x
      def empty(self):
         return len(self.items) == 0

Language prop

Propiedades del lenguaje:

* Todo es un objeto
* Módulos, clases, funciones
* Manejo de excepciones
* Mecanografía dinámica, polimorfismo
* Alcance estático
* Operación de sobrecarga
* Estructura de bloque de sangría

- De lo contrario, la sintaxis convencional

Tipos de datos de alto nivel:

Números: int, largo, flotante, complejo.

Cuerdas: inmutables

Listas, diccionarios: contenedores

Otros tipos por ej. datos binarios, expresiones regulares, instrospección

Los módulos de extensión pueden definir nuevos tipos de datos "incorporados"

Propiedades de alto nivel:

* Extremadamente portátil:

- Unix, Windows, Mac, Palm, VxWorks, Play Station, ...

* Compila el código de bytes interpretado

- la compilación es implícita y automática

* Gestión de memoria mediante recuento de referencias

- más fácil para extensiones C / C ++

"Seguro": sin volcados del núcleo

Propiedades de tacto libre

* Gratis (código abierto, no GPL)
* Maduro (9 años)
* Comunidad de usuarios de apoyo y más libros en la tubería

* Diseño elegante, fácil de aprender.

- se lee como "pseudocódigo"
- Adecuado como primer idioma

¿Qué es python?

O-O lenguaje de creación rápida de prototipos
No solo como lenguaje de script
No solo como otro aspirante a Perl
Extensible (agregar nuevos módulos)
- C / C ++ / Fortran / lo que sea
- Java (a través de JPython)
* Integrable en aplicaciones

Lo que hay en un nombre?

* A pesar de los logotipos de las serpientes y la mascota, lleva el nombre de Monty Python Flying's Circus
* Las personas con problemas de humor pueden ignorar con seguridad las referencias de spam :-)
* Nadie espera la inquisición española

2008


2005

Por qué inventó Python?

Hace 30 años atrás

Descubrió las computadoras y la programación
Aprendió Algol-60, Fortran y Pascal (en ese orden!)
Disfrutó la programación más que cualquier otra cosa
Quería mejores herramientas y empecé creando ellas
Empecé compartiendo con otros
Empecé aprendiendo acerca de otros lenguajes

Hace 20 años atrás

Conocí C y UNIX muy bien.
Trabajé en una implementación de ABC, un nuevo lenguaje
Porté este a Mac y PC (DOS).
Fue realmente emocionante por la filosofía de los autores
Fue decepcionante por la falta integración con ABC
Ví su fracaso para ganar aceptación.

Hace 15 años atrás

Trabajé en la implementación de Amoeba, un nuevo OS.
Encontré que necesitaba un lenguaje de scripting
"Para cerrar la brecha entre el shell y C"
Quería la elegancia de ABC en ese lenguaje de scripting
Tenía algunas ideas en mis manos
Tenía algunas ideas en la implementación del lenguaje

Nacimiento de Python
Christmas de 1989
aproximadamente dos semanas de tiempo libre sin planes
lo tenia todo en mi cabeza
Mac mac con velocidad de la luz C en disco duro de 20 MB
primer código: un generador de análisis
seguido pronto: un programa en ejecución!

Algunos objetivos de diseño:

Como Shell (solicitud interactiva y archivos de script)
Arquitectura extensible (crear nuevos tipos)
Una herramienta entre muchas, funciona bien con otras
Funcionalidad adicional implementable por separado
evitar el síndrome de no inventado aquí (pedir prestado libremente)
factible como un proyecto de una persona (corta algunas esquinas)

¿Por qué orientado a objetos?

* Una palabra: extensibilidad
* El diseño original era OO dentro de la notación OO utilizada para el acceso al método, pero no admitía clases definidas por el usuario.
* El acceso al método se generalizó a espacios de nombres.
* Un espacio de nombres unificado para todo en un módulo
* Cada objeto es un espacio de nombres por derecho propio
* Un módulo es solo otro objeto
* La búsqueda de nombres se personaliza por espacio de nombres.
* Las clases definidas por el usuario se agregaron dentro del primer año, pero durante mucho tiempo se mantuvieron como ciudadanos de segunda clase (hasta clases de estilo nuevo en Python 2.2)


cosas buenas sobre ABC

* Cinco tipos de datos potentes:

- lista, tabla, tupla, número, texto

* La ortogonalidad es un principio importante
* Sin límites: los valores pueden ser tan grandes como los ajustes en la memoria
* Los números representan valores matemáticos, no bits
* Potente procesamiento de cadenas incorporado
* No hay declaraciones de tipo; la asignación crea variables
* Estructuras de control simples: SI, SELECCIONAR, MIENTRAS, PARA
* Mensaje interactivo >>>

cosas no tan buenas sobre ABC

* Implementación monolítica; difícil de agregar cosas nuevas
* difícil de interactuar con el sistema de archivos
* reinventado la terminología de programación
* Apostrophe (') en identificadores; MAYÚSCULAS palabras clave
* La lista es un conjunto múltiple sin ordenar (bolsa); la mesa está ordenada
* Cuerdas pequeñas relativamente lentas
* Asignación: PONER expresión en variable
* Sin manejo de errores (los errores vuelven al >>> indicador)
* El entorno de edición está demasiado integrado.
* Apareció más a personas con acceso insuficiente a la computadora; difícil de establecer una comunidad de "adoptantes anteriores" en la palabra Unix debido a la falta de integración del sistema operativo.


Lo que cambié

Extensibilidad una piedra angular de implementación
Archivos incorporados; otra funcionalidad del sistema operativo en una extensión
Regresó a la terminología de programación estándar
Subrayar en identificadores; palabras clave en minúsculas
La lista tiene orden; diccionario es tabla hash
Optimizar para cadenas pequeñas (memcpy es muy rápido)
Asignación: variable = expresión
* Se agregaron excepciones, try / except, try / finally (Modula 3)
* Se eliminó el entorno de edición integrado
* Apele al mundo UNIX: imite el comportamiento / bin / sh, # para comentarios, soporte # !, acceda a la mayoría de las llamadas de sistema Unix de bajo nivel (¡pero también a puertos PC / Mac!)


¿Qué más he cambiado?

* Gran parte de esta forma está motivada por mantenerlo lo suficientemente simple como para ser un objeto de una sola persona
* Inferencia de tipo descartada; escritura dinámica en su lugar
* Caída de estructuras de control de "refinamiento"
* Tipos numéricos int, largos y flotantes separados
* Errores más grandes:

- Enteros de 32 bits (optimización prematura)
- int / int truncando el valor (C copiado sin sentido)
- dicotomía clase / tipo (clases de usuario a posteriori)
- excepciones de cadena (¡las excepciones llegaron antes de las clases!)

Lenguaje orientado a objetos tipificado dinámicamente
Los programas de Python parecen un pseudocódigo ejecutable
Soporta múltiples paradigmas:
procesal, orientado a objetos, algunos funcionales
Tipos de datos de alto nivel y espacios de nombres
Un poco como Lisp y Smalltalk
Extensible en lenguajes de nivel inferior (C, Fortran, ...)
¡Ese es el origen de su naturaleza OO!
Problemas más controvertidos:
estructura de bloque mediante sangría
comprobación dinámica de tipos
Escrito en portable ANSI C.


In this codelab you'll learn the basic "Hello World" of machine
learning where, instead of programming explicit rules in a language
such as Java or C++, you'll build a system that is trained on data
to infer the rules that determine a relationship between numbers.

Consider the following problem: You're building a system that performs
activity recognition for fitness tracking. You might have access to
the speed at which a person is moving, and attempt to infer their
activity based on this speed using a conditional:

En esta sección aprenderá el "Hola mundo" básico del aprendizaje
automático donde, en lugar de programar reglas explícitas en un
lenguaje como Java o C ++, creará un sistema que está capacitado
en datos para inferir las reglas que determinan un relación entre
números.

Overview

In this codelab, you will learn how to build and train a neural
network that recognises handwritten digits. Along the way, as you
enhance your neural network to achieve 99% accuracy, you will also
discover the tools of the trade that deep learning professionals
use to train their models efficiently.


This codelab uses the MNIST dataset, a collection of 60,000 labeled
digits that has kept generations of PhDs busy for almost two decades.
You will solve the problem with less than 100 lines of Python / TensorFlow
code.

What you'll learn
   How to add more layers
What you'll need
Just a browser. This workshop can be run entirely with Google Colaboratory.
Feedback
Please tell us if you see something amiss in this lab or if you think
it should be improved. We handle feedback through GitHub issues
[feedback link].

.. note::

   "Broadcasting" is a standard trick used in Python and numpy, its
   scientific computation library. It extends how normal operations
   work on matrices with incompatible dimensions.
   "Broadcasting add" means "if you are adding two matrices but you
   cannot because their dimensions are not compatible, try to
   replicate the small one as much as needed to make it work."

   "Broadcasting" es un truco estándar utilizado en Python y numpy,
   su biblioteca de computación científica. Extiende cómo funcionan
   las operaciones normales en matrices con dimensiones incompatibles.
   "Agregar difusión" significa "si está agregando dos matrices pero
   no puede porque sus dimensiones no son compatibles, intente replicar
   la pequeña tanto como sea necesario para que funcione".


`Notebook <https://jupyter.org>`_

The Notebook is an open-source web application that allows you to
create and share documents that contain live code, equations,
visualizations and narrative text. Uses include: data cleaning and
transformation, numerical simulation, statistical modeling, data
visualization, machine learning, and much more.

In this section, we present the basic features of notebooks. See the
User Interface Tour in the Help menu in the Jupyter notebook.

`Docs <https://jupyter-notebook.readthedocs.io/en/stable/notebook.html>`_
Markdown
LaTeX
Mathematics Inline and Display


.. _righttriangle:

.. proof:definition:: Right triangle

   A *right triangle* is a triangle in which one angle is a right angle.

.. _pythagorean:

.. proof:theorem:: Pythagorean theorem

   Let :math:`a`, $b$, and $c$ denote the length of the sides of a
   :ref:`righttriangle`, i.e. of a triangle with one angle equal to 90°.
   Without loss of generality, assume that $a ≤ b ≤ c$. Then

   .. math::

      a^2 + b^2 = c^2

   the square of the hypotenuse is equal to the sum of the squares of the other two sides.

.. _proof:

.. proof:proof::

   The $x$.

You can label and reference definition and theorems (e.g. :numref:`theorem {number} <pythagorean>`). You can also reference proofs (see the :ref:`proof of the Pythagorean theorem <proof>`).

.. parsed-literal::

   val :math:`\alpha` = 10;


Numbers:

In [7]: z = complex(1, 2)

In [8]: z
Out[8]: (1+2j)

In [9]: z.real
Out[9]: 1.0

In [10]: z.imag
Out[10]: 2.0

In [11]: z.conjugate()
Out[11]: (1-2j)

import cmath
cmath.sqrt(-1)
cmath.polar(z)
abs(z)
cmath.phase(z)
cmath.rect(r, phi)
float('inf')
float('nan')
cmath.isinf(complex(0.0, math.inf))
cmath.isnan(complex(math.nan, 0.0))
help(math.isclose)
def exp(x):
    ...:     return sum([x**n / math.factorial(n) for n in range(0, 100)])

https://docs.python.org/3/reference/lexical_analysis.html#keywords


https://docs.python.org/3/library/functions.html
https://pep8.org

~ » ipython                                                                                                                                              carlosal1015@Oromion
Python 3.8.2 (default, Apr  8 2020, 14:31:25)
Type 'copyright', 'credits' or 'license' for more information
IPython 7.14.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: x = 2

In [2]: whos
Variable   Type    Data/Info
----------------------------
x          int     2

today = today = (2020,2,21)
year, month, day = today
https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions

list1 = [('eggs',5.25),('honey',9.70),('carrots',1.10),('peaches','2.45'),(,)]


Python 2 vs

https://devguide.python.org/#status-of-python-branches
