~~~~~~~~~~~~~~~~~~
Fibonacci sequence
~~~~~~~~~~~~~~~~~~

La sucesión

.. math::

    1, 1, 2, 3, 5, 8, 13, \ldots

es conocida como la *sucesión de Fibonacci* y sus números con frecuencia
ocurren en la naturaleza. Lleva el nombre del comerciante y matemático
italiano (Leonardo de Pisa, 1170-1250). Esta sucesión tiene la propiedad
que, *después* de los primeros dos términos, cada término sucesivo es
la suma de los dos términos precedentes.

.. math::

    \begin{aligned}
    t_1 &= 1\\
    t_2 &= 1\\
    t_3 &= 2 = t_1 + t_2\\
    t_4 &= 3 = t_2 + t_3\\
    t_5 &= 5 = t_3 + t_4\\
    t_6 &= 8 = t_4 + t_5\text{ and so on}.
    \end{aligned}

Fibonacci propuso esta sucesión como una forma de modelar el crecimiento
en el número de conejos producidos a partir de un solo par de conejos
reproductores.

=============================================
Recurrece relation for the Fibonacci sequence
=============================================

.. math::

    t_1 = 1\text{ and } t_2 = 1\quad t_n = t_{n-2} + t_{n-1},\quad n\geq 3.