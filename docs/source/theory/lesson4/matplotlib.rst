==========
Matplotlib
==========

.. math::

   \begin{aligned}
      f\left(x\right)
      & =f\left(x_{0}\right)+f^{\prime}\left(x_{0}\right)\left(x-x_{0}\right)+\frac{1}{2}f^{\prime\prime}\left(x_{0}\right){\left(x-x_{0}\right)}^{2}+\cdots \\
      & +\frac{1}{n!}f^{\left(n\right)}\left(x_{0}\right){\left(x-x_{0}\right)}^{n}+\frac{1}{\left(n+1\right)!}f^{\left(n+1\right)}\left(z\right){\left(x-x_{0}\right)}^{n+1}
   \end{aligned}

.. math::

   f(x) \doteq p(x) \equiv f\left(x_{0}\right)+f^{\prime}\left(x_{0}\right)\left(x-x_{0}\right)+\cdots+\frac{1}{n !} f^{(n)}\left(x_{0}\right)\left(x-x_{0}\right)^{n}

====================
Lagrange Polynomials
====================

If $x_{0},x_{1},\ldots,x_{n}$ are distinct points, we can define the *Lagrange polynomials*

.. math::

   \begin{aligned}
      l_{j}\left(x\right)
      & =\frac{\left(x-x_{0}\right)\left(x-x_{1}\right)\cdots\left(x-x_{j-1}\right)\left(x-x_{j+1}\right)\cdots\left(x-x_{n}\right)}{\left(x_{j}-x_{0}\right)\left(x_{j}-x_{1}\right)\cdots\left(x_{j}-x_{j-1}\right)\left(x_{j}-x_{j+1}\right)\cdots\left(x_{j}-x_{n}\right)} \\
      & =\prod^{n}_{\substack{k=0                                                                                                                                                                                                                                              \\k\neq j}}\left(\frac{x-x_{k}}{x_{j}-x_{k}}\right),\quad j=0,1\ldots,n.
   \end{aligned}

It is easy to verify that these polynomials, which are all of degree $n$, satisfy

.. math::

   l_{j}\left(x_{i}\right)=\begin{cases}1, & \text{if }i=j\\0, & \text{if }i\neq j.\end{cases}

.. math::

   I\left(f\right)=\int\limits^{b}_{a}f\left(x\right)\,\mathrm{d}x

.. math::

   \int\limits^{b}_{a}f\left(x\right)\,\mathrm{d}x\doteq \sum_{i=0}^{n}\alpha_{i}f\left(x_{i}\right)

=========================
The Newton-Cotes Formulas
=========================

In the *rectangle rule*, $f$ is approximated by its value at the end point $a$ (or, alternatively, at $b$) so that

.. math::

   I\left(f\right)\doteq R\left(f\right)=\left(b-a\right)f\left(a\right).

We could also approximate $f$ by another constant obtained by evaluating $f$ at a point interior to the interval; the most common choice is $\left(a+b\right)/2$, which gives the *midpoint rule*

.. math::

   I\left(f\right)\doteq M\left(f\right)=\left(b-a\right)f\left(\frac{a+b}{2}\right).

.. math::

   I\left(f\right)\doteq T\left(f\right)=\frac{\left(b-a\right)}{2}\left[f\left(a\right)+f\left(b\right)\right].

.. math::

   I\left(f\right)\doteq S\left(f\right)=\frac{\left(b-a\right)}{6}\left[f\left(a\right)+4f\left(\frac{a+b}{2}\right)+f\left(b\right)\right]

.. math::

   \frac{1}{6}\left[f\left(a\right)+4f\left(\frac{a+b}{2}\right)+f\left(b\right)\right]=\frac{1}{3}\left[\frac{f\left(a\right)+f\left(b\right)}{2}\right]+\frac{2}{3}f\left(\frac{a+b}{2}\right).

.. math::

   I\left(f\right)=\int_{a}^{b}f\left(x\right)\,\mathrm{d}x=\sum_{i=1}^{n}\int_{x_{i-1}}^{x_{i}}f\left(x\right)\,\mathrm{d}x.

.. math::

   I_{CM}\left(f\right)=\sum_{i=1}^{n}h_{i}f\left(\frac{x_{i}+x_{i-1}}{2}\right)

.. math::

   I_{CT}\left(f\right)=\sum_{i=1}^{n}\frac{h_{i}}{2}\left[f\left(x_{i-1}\right)+f\left(x_{i}\right)\right]

.. math::

   I_{CS}\left(f\right)=\frac{1}{6}\sum_{i=1}^{n}h_{i}\left[f\left(x_{i-1}\right)+4f\left(\frac{x_{i-1}+x_{i}}{2}\right)+f\left(x_{i}\right)\right].
