****************
NumPy |:abacus:|
****************

============
:mod:`numpy`
============

--------------
:class:`array`
--------------

.. class:: array(object, dtype=None, copy=True, order='K', subok=False, ndmin=0):

    :object: array_like
    :type user: string
    :pdtype: data-type, optional
    :copy: bool, optional
    :order: {'K', 'A', 'C', 'F'}, optional
    :subok: bool, optional
    :ndmin: int, optional
    :type config: OdorikConfig

    Create an array.

=================
The parrot module
=================

.. testsetup:: *

   import parrot

The parrot module is a module about parrots.

Doctest example:

.. doctest::

   >>> parrot.voom(3000)
   This parrot wouldn't voom if you put 3000 volts through it!

Test-Output example:

.. testcode::

   parrot.voom(3000)

This would output:

.. testoutput::

   This parrot wouldn't voom if you put 3000 volts through it!