import numpy as np

class Integrator():
    """A class to integrate functions numerically.
       Create an object by Integrator(a,b,N)
       a = Interval lowerbound
       b = Interval upperbound
       N = Number of subintervals
    """
    def __init__(self,f,a,b,N):
        """ (Integrator, a, b, N) -> NoneType
        Create an Integrator with N intervals.
        """
        self.x = np.linspace(a,b,N+1)    
        self.h = (b - a)/N
        self.f = f;

    def midpoint(self):
        """ (Integrator) -> float
        Numerical integration by midpoint rule
        """
        s=(self.x[1:] + self.x[:-1])/2
        y = self.f(s)
        T = (self.h) * np.sum(y[:])
        return T

    def trapez(self):
        """ (Integrator) -> float
        Numerical integration by trapez rule
        """
        y = self.f(self.x)
        y_right = y[1:] # right endpoints
        y_left =   y[:-1] # left endpoints
        T = (self.h/2) * np.sum(y_right + y_left)
        return T

    def simp(self):
        """ (Integrator) -> float
        Numerical integration by Simpson rule
        """
        S = (2/3)*self.midpoint() + (1/3)*self.trapez()
        return S


errmid = np.zeros( 5 )
for i in range(5):
	quad = quadrature.Integrator(np.exp, 0, 1, pow(2,i)*10)
	errmid[i] = np.fabs(exactintegral - quad.midpoint())

np.log(errmid[:-1] / errmid[1:])/np.log(2)
