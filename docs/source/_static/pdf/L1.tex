% arara: clean: {
% arara: --> extensions:
% arara: --> ['log','aux','synctex.gz',
% arara: --> 'toc','vrb','snm','nav','pdf']
% arara: --> }
% arara: lualatex: {
% arara: --> interaction: batchmode
% arara: --> }
% arara: lualatex: {
% arara: --> synctex: yes,
% arara: --> interaction: batchmode
% arara: --> }
% arara: clean: {
% arara: --> extensions:
% arara: --> ['log','aux','synctex.gz',
% arara: --> 'toc','vrb','snm','nav']
% arara: --> }
\documentclass[handout]{beamer}

\usetheme{focus}
\usepackage{bera}

\usepackage{textcomp}
\usepackage{listings}

\lstdefinestyle{python}{
	basicstyle=\small\ttfamily,
	language=python,
	backgroundcolor=\color{lime},
	numbers=none,
	upquote=true,
	escapeinside=||,
	tabsize=2
}

\title{Introduction \\to Scientific Computing}
\subtitle{with Python}
\author{Utku Kaya}
\institute{Department of Mathematics\\ University of Kiel, Germany}
\date{24-28 02 2020}

\begin{document}

\begin{frame}
\maketitle
\end{frame}

\section*{Getting started}
\subsection{What is scientific computing?}

\begin{frame}[plain]{}
	\begin{center}
	\includegraphics[scale=0.35]{Drawing-6}
	\end{center}
\end{frame}

\begin{frame}[plain]{}
	\begin{center}
	\includegraphics[scale=0.35]{Drawing-2}
	\end{center}
\end{frame}

\begin{frame}[plain]{}
	\begin{center}
	\includegraphics[scale=0.35]{Drawing-3}
	\end{center}
\end{frame}

\begin{frame}[plain]{}
	\begin{center}
	\includegraphics[scale=0.25]{Drawing-5}
\end{center}
	\pause
	Scientific computing covers the collection of tools, techniques and
	theories \pause required to solve on a computer \pause mathematical
	models of problems in science and engineering.
	\footnote{Golub, Ortega, 2014. Scientific Computing: An Introduction with Parallel Computing.}
\end{frame}

\subsection{What is Python?}

\begin{frame}[plain]{Basic features of Python}
	Python is an interpreted, high-level, general-purpose programming
	language.\\[\baselineskip]
	\pause
	Released in 1991,  Guido van Rossum (Netherlands).\\[\baselineskip]
	\pause
	The Zen of Python, by Tim Peters:

	\begin{itemize}
		\item Beautiful is better than ugly.
		\pause
		\item Explicit is better than implicit.
		\pause
		\item Simple is better than complex.
		\pause
		\item Complex is better than complicated.
		\pause
		\item Readability counts.
		\item \ldots
	\end{itemize}
\end{frame}

\section{Course Overview}

\begin{frame}{Course Overview}
Today: Introduction, syntax, variables, data types, operators, built-in mathematical functions,\\[\baselineskip]
\pause
25.02: Functions, conditional statements, recursion, classes/objects, iterators, reading \& writing files,\\[\baselineskip]
\pause
26.02: NumPy introduction, numerical differentiation and integration,\\[\baselineskip]
\pause
27.02: Solving systems of linear equations,\\[\baselineskip]
\pause
28.02: Example: a finite difference code for Poisson equation in 2D.
\end{frame}

\begin{frame}[fragile]{First lines of code}
\begin{small}
\begin{lstlisting}[style=python]
>>> print("Hello, Peru!")
\end{lstlisting}
\end{small}
The \lstinline[style=python]|>>>| symbol is called a prompt.
\pause
\begin{lstlisting}[style=python]
>>> person = input('Enter your name: ') |\pause|
>>> print('Hello ', person, '!') |\pause|
>>> print('Hello ' + person + '!') |\pause|
>>> print('Hello, {}!'.format(person)) |\pause|
>>> print('Hello ', person, '!', sep='')
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{First lines of code}
Always put a space before and after every binary operator!
\begin{lstlisting}[style=python]
>>> num = 2
\end{lstlisting}
\lstinline[style=python]|num| is assigned \lstinline[style=python]|2|.
\begin{lstlisting}[style=python]
>>> 2 = num
\end{lstlisting}
Assignment is not symmetric!
\end{frame}

\begin{frame}[fragile]{First lines of code}
\begin{lstlisting}[style=python]
>>> num = 2 |\pause|
>>> print(num) |\pause|
>>> print("num =",num) |\pause|
>>> print( type(num)) |\pause|
>>> print("The number ", num, " is of type", type(num)) |\pause|
>>> num = 3.0 |\pause|
>>> print(isinstance(num, int)) |\pause|
>>> num = 3 + 5j |\pause|
>>> print(isinstance(num, complex)) |\pause|
>>> num = float(-1) |\pause|
>>> print(num)
\end{lstlisting}
Note: a float type number can have precision up to 15 decimal places.
\end{frame}

\begin{frame}[fragile]{Reassigning to Variables}
\begin{lstlisting}[style=python]
>>> num = 2
>>> num1 = 3 * num
>>> num1  |\pause|
>>> num = 4 |\pause|
>>> num1
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Memory Addresses}
Python remembers and reuses some objects.
\begin{lstlisting}[style=python]
>>> id(3)	 |\pause|
>>> help(id)	 |\pause|
>>> x = 3	 |\pause|
>>> id(x)	 |\pause|
>>> id(3.1)	 |\pause|
>>> id(300)	 |\pause|
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Strings}
\begin{lstlisting}[style=python]
>>> txt = "Some text"	|\pause|
>>> len(txt)	|\pause|
>>> len(txt) == 12	|\pause|
>>> txt = """This is a string
... in two lines"""	|\pause|
>>> print(txt)	|\pause|
>>> print(type(txt))	|\pause|
>>> first_2_chars = txt[:2]	|\pause|
>>> print(first_2_chars)	|\pause|
>>> last_2_chars = txt[-2:]	|\pause|
>>> print(last_2_chars)	
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Multiple assignments}
Following lines are equivalent.
\begin{lstlisting}[style=python]
>>> x, y = 10, 20 |\pause|
>>> x, y = (10, 20) |\pause|
>>> (x, y) = 10, 20 |\pause|
>>> (x, y) = (10, 20) |\pause|
\end{lstlisting}
Multiple assignments work for strings, too.
\begin{lstlisting}[style=python]
>>> x, y = 'je'  |\pause|
>>> x  |\pause|
>>> y |\pause|
>>> x = y = 'je'  |\pause|
>>> print(x, y) |\pause|
>>> print(x * 3)
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Arithmetic Operators}
\begin{lstlisting}[style=python]
>>> x, y = -3, 4	 |\pause|
>>> x + y			# Addition		 |\pause|
>>> x - y			# Subtraction		 |\pause|
>>> x * y			# Multiplication	 |\pause|
>>> x / y			# Division	 |\pause|
>>> x % y			# Modulus	 |\pause|
>>> x ** y		# Exponentiation	 |\pause|
>>> x // y		# Floor division	 |\pause|
>>> -3 ** 2	 |\pause|
>>> -(3 ** 2)	 |\pause|
>>> (-3) ** 2	 |\pause|
>>> print('{0} + {1} = {2}'.format(x, y, x + y))
\end{lstlisting}
It holds \lstinline[style=python]|x = (x // y) * y + (x % y)|.
\end{frame}

\begin{frame}[fragile]{Numeric Precision}
\begin{lstlisting}[style=python]
>>> 2 / 3 + 1	|\pause|
>>> 5 / 3	|\pause|
>>> .1 + .1 + .1 == .3	|\pause|
\end{lstlisting}
Bruce M. Bush, \textit{Programming with the Perils:}\\[\baselineskip]
There are no easy answers. \pause It is the nature of binary
floating-point to behave the way I have described. \pause In order to
take advantage of the power of computer floating-point, you need to
know its limitations and work within them.
\end{frame}

\begin{frame}[fragile]{Assignment Operators}
\begin{lstlisting}[style=python]
>>> x = 13	|\pause|
>>> x += 5	|\pause|
>>> x -= 3	|\pause|
>>> x *= 2	|\pause|
>>> x /= 2		|\pause|
>>> x %= 3	|\pause|
>>> x //= 3	|\pause|
>>> x **= 3	|\pause|
>>> x &= 3	|\pause|
>>> x |= 3
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Assignment Operators}
\begin{lstlisting}[style=python]
>>> x = 2
>>> x *= 3 + 4
>>> x
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Multiple Lines}
\begin{lstlisting}[style=python]
>>> (2
... + 3) |\pause|
>>> 2 + \
... 3
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Comparison Operators}
\begin{lstlisting}[style=python]
>>> x, y = 13, 9 	|\pause|
>>> x == y	|\pause|
>>> x != y	|\pause|
>>> x > y	|\pause|
>>> x >= y	|\pause|
>>> x < y	|\pause|
>>> x <= y
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Logical Operators and Identity Operators}
\begin{lstlisting}[style=python]
>>> x, y = 13, 9 |\pause| 
>>> x < 5	|\pause|
>>> y < 10	|\pause|
>>> x < 5 and y < 10		|\pause|
>>> x < 5 or y < 10	|\pause|
>>> not(x < 5 and y < 10)	|\pause|
>>> x is y	|\pause|
>>> x is 13	|\pause|
>>> x is not 4	|\pause|
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Membership Operators}
\begin{lstlisting}[style=python]
>>> 'abc' in 'abcd'		|\pause|
>>> 'abc' in 'Abcd'	|\pause|
>>> 'abc' not in 'Abcd'
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Built-in Functions}
\begin{lstlisting}[style=python]
>>> abs(-9)	|\pause|
>>> abs(3 - 5)	|\pause|
>>> pow(2, 3)	|\pause|
>>> pow(2, abs(-3))	|\pause|
>>> pow(2, abs(round(-3.6)))	|\pause|
>>> help(abs)	|\pause|
>>> round(3.141592653, 2)	|\pause|
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Built-in math functions}
Tell Python that you want to use functions in module math:
\begin{lstlisting}[style=python]
>>> import math	|\pause|
>>> help(math)	|\pause|
>>> math.fabs(-2.1)	|\pause|
>>> x = 2*math.pi	|\pause|
>>> math.sin(x)	|\pause|
>>> math.sqrt(9)
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Built-in math functions}
If you need only several functions from \lstinline[style=python]|math| module:
\begin{lstlisting}[style=python]
>>> from math import pi, sqrt
>>> help(sqrt)
>>> sqrt(9)
\end{lstlisting}
\end{frame}

\begin{frame}[focus]
Thanks for your attention!
\end{frame}

%\begin{frame}[fragile]{For loops}
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>>for count in [1, 2, 3]: 
%...		print(count)
%>>>for i in range(10):
%...     print(i)
%>>>for color in ['red', 'blue', 'green']:
%...		print(color)
%>>> num=1
%...		for color in ['red', 'blue', 'green']:
%...		print(color,num)
%...		num+=1
%\end{lstlisting}\end{small}
%
%\end{frame}
%    \begin{frame}[fragile]{While loops}
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>>i=0
%>>>while (i<10):
%...     print(i)
%...	    i+=1
%>>>i=0
%>>>while True:
%...     print(i)
%...	    i+=1
%...	    if i==10:
%...	        break
%\end{lstlisting}\end{small}
%
%\end{frame}
%  
% \begin{frame}[fragile]{If statements}
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>> x=20
%>>> y=19
%>>> if x>y:
%...		print(x,'is greater than', y)
%>>> if x>y:
%...		print(x,'is greater than', y)
%    else:
%...		print(y,'is greater or equal to', x)	
%>>> if (x%2==0) and (x%3==0):
%...  print(x,'can be divided by 6')
%...else:
%...	 print(x,'cannot be divided by 6')
%\end{lstlisting}\end{small}
% \end{frame}
%  
%\begin{frame}[fragile]{Booleans}
%Boolean in Python can have two values: True or False.
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%condition = False
%if condition == True:
%    print("The condition is True")
%else:
%    print("The condition is False")
%\end{lstlisting}\end{small}
% Note following equivalence:
% \begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
% if condition == True:
% if condition:
% \end{lstlisting}\end{small}
%  \end{frame}
%
%    \begin{frame}[fragile]{Lists}
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>> assorted_list = [True, False, 1, 1.1, 1+2j, 'Learn', b'Python']
%>>> first_element = assorted_list[0]
%>>> print(first_element)
%>>> print(assorted_list)
%>>> for item in assorted_list:
%			print(type(item))
%>>> nested = [[1,1,1], [2,2,2], [3,3,3]]
%>>> for items in nested:
%			for item in items:
%				print(item, end=' ')
% \end{lstlisting}\end{small}
%  \end{frame}
% 

%
%\begin{frame}[fragile]{Lists}
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime},numbers=none]
%>>>animals = ['dog','cat','sheep','alpaca']  |\pause|
%>>>sorted(animals) |\pause|
%>>>len(animals) |\pause|
%>>>len(animals[1]) |\pause|
%>>>animals[1]
% \end{lstlisting}\end{small}
%\end{frame}
% \begin{frame}[fragile]{Fibonacci}
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>> a, b = 0, 1
%>>> while a < 1000:
%...		print(a, end=',')
%...		a, b = b, a+b
% \end{lstlisting}\end{small}
%  \end{frame}
%  
% \begin{frame}[fragile]{Guess which number!}
% Save following code in game.py and run
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%import random
%number = random.randint(1, 10)
%niter=0
%print('Guess which number I chose between 1 to 10!')
%while True:
%    guess=int(input('Your guess:'))
%    niter+=1
%    if guess>number:
%        print('Too high')
%    elif guess<number:
%        print('Too low')
%    elif guess==number:
%        print('Needed {} iterations'.format(niter))
%        break
% \end{lstlisting}\end{small}
%  \end{frame}  
% 
%   \begin{frame}[fragile]{Python Classes}
%Python is an object oriented programming language.\\
%Almost everything in Python is an object, with its properties and methods.\\
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>> s = 'Hello!'
%>>> s.upper()
%>>> s
%>>> s2 = s.upper()
%>>> s2
%>>> s
%>>> text = 'AAaaAA'
%>>> text.count('AA')
%>>> text.lower.count('a')
%>>> text.count('aA')
% \end{lstlisting}\end{small}
% \end{frame}
%    
%   \begin{frame}[fragile]{Python Classes}
%A Class is like an object constructor, or a "blueprint" for creating objects.\\
%Create a class named MyClass, with a property named n:
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>> class MyClass:
%...		n=5
% \end{lstlisting}\end{small}
% Create an object named LaMolina, and print the value of n:
% \begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>> LaMolina = MyClass()
%...		print(LaMolina.n)
%>>> LaMolina.learningPython=True
%>>> type(LaMolina.learningPython)
%>>> LaMolina.learningPython
% \end{lstlisting}\end{small}
%  \end{frame}
%  
%  
%\begin{frame}[fragile]{Python Classes}
%All classes have a function called %__init__(), 
%init which is always executed when the class is being initiated.\\
%Use the init %__init__()
% function to assign values to object properties, or other operations that are necessary to do when the object is being created:
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>>class Person:
%...		def __init__(self, name, age):
%...				self.name = name
%...				self.age = age
%		
%p1 = Person("Dandy", "Very young")
%
%print(p1.name)
%print(p1.age)
%\end{lstlisting}\end{small}
%\end{frame}
%  
%\begin{frame}[fragile]{Python Classes}
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>>class Person:
%...		def __init__(self, name, age):
%...				self.name = name
%...				self.age = int(age)
%		
%		
%p1 = Person("Dandy", "Very young")
%p1 = Person("Dandy", "35")
%
%print(p1.name, p1.age)
%\end{lstlisting}\end{small}
%\end{frame}
%
%\begin{frame}[fragile]{Python Classes}
%Objects can also contain methods. Methods in objects are functions that belong to the object.
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>>class Person:
%...		def __init__(self, name, age,field ):
%...				self.name = name
%...				self.age = int(age)
%...				self.field = field		
%...		def introduce(self):
%...				print("Hello my name is " + self.name+ \
%...						". I am " , self.age ,\
%...					     "years old. My field is " + self.field )
%		
%p1 = Person("Dandy", "35")
%p1 = Person("Dandy", "35", "Math.")
%p1.introduce()
%\end{lstlisting}\end{small}
%\end{frame}
%
%\begin{frame}[fragile]{Python Classes}
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>>class Car:
%...    def __init__(self, b, c, i):
%...    self.brand= b
%...    self.color= c
%...    self.iselectric= i
%		
%...    def makeElectric(self):
%...    self.iselectric= True
%		
%...    def makeDiesel(self):
%...    self.iselectric= False
%\end{lstlisting}\end{small}
%\end{frame}
%
%\begin{frame}[fragile]{Python Classes}
%The self parameter is a reference to the current instance of the class, and is used to access variables that belongs to the class.\\
%It does not have to be named self , you can call it whatever you like, but it has to be the first parameter of any function in the class:\\
%You can delete properties and also objects with the del command.
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>>p1.age(40)
%>>>del p1.age
%>>>p1.age(40)
%>>>del p1
%\end{lstlisting}\end{small}
%
%\end{frame}
%
%\begin{frame}[fragile]{Reading files}
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>>file = open('file_example.txt', 'r')
%>>>contents = file.read()
%>>>print(contents)
%>>>file.close()
%\end{lstlisting}\end{small}
%open -> opens a file and returns an object that knows how to get information from the file\\
%r -> read\\
%w -> write\\
%a -> append
%\end{frame}
%
%\begin{frame}[fragile]{Reading files}
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>> import os
%>>> os.getcwd()
%>>> os.chdir('...') 
%>>> os.getcwd()
%\end{lstlisting}\end{small}
%... is \textit{the directory where the file\_example.txt is saved}
%\begin{small}\begin{lstlisting}[language=python,tabsize=1,backgroundcolor=\color{lime}]
%>>>file = open('file_example.txt', 'r')
%>>>contents1 = file.read(10)
%>>>contents2 = file.read()
%>>>print(contents1)
%>>>print(contents2)
%>>>file.close()
%\end{lstlisting}\end{small}
%Method call file.read(10) moves the file cursor, so the next call, file.read(), reads everything from character 11 to the end of the file.
%\end{frame}
\end{document}