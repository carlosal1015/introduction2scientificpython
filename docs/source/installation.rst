*******************************************************
Installation of Python and Python packages |:computer:|
*******************************************************

There are several Python `implementations <https://en.wikipedia.org/wiki/Programming_language_implementation>`_
such as `Jython <https://www.jython.org>`_, `MicroPython <https://micropython.org>`_,
`CircuitPython <https://circuitpython.org>`_, `PyPy <https://www.pypy.org>`_,
`RustPython <https://rustpython.github.io/demo>`_ and more.
We use the traditional `CPython <https://github.com/python/cpython>`_.

.. tip::

  If for some reason you have not been able to install Python 3, it
  is possible to practice the exercises of the course thanks to this
  |blinder| (internet connection is required, might take eight minutes
  to start, please be patient).

.. |blinder| image:: https://mybinder.org/badge_logo.svg
  :target: https://mybinder.org/v2/gl/carlosal1015%2Fintroduction2scientificpython/master?urlpath=lab/tree/src/notebook

.. tabs::

  .. tab:: |linux|

    Please open the `terminal emulator <https://en.wikipedia.org/wiki/Terminal_emulator>`_
    and according to the `package manager <https://en.wikipedia.org/wiki/Package_manager>`_
    of your distribution and run one of these lines.

    .. code:: console

      [user@hostname ~]$ sudo dnf install python38   # Fedora, RHEL, Clear Linux.
      [user@hostname ~]$ sudo apt install python3    # Debian, deepin, elementary, Raspbian.
      [user@hostname ~]$ sudo pacman -Sy python      # Arch Linux, Manjaro.
      [user@hostname ~]$ sudo zypper install python3 # OpenSUSE (Tumbleweed, Leap), SUSE.

  .. tab:: |docker|

    Docker is a free software platform used for packaging software into
    standardized units for development, shipment and deployment.

    .. figure:: _static/img/docker.png
      :align: center
      :alt: ABC.

    `This <https://hub.docker.com/r/carlosal1015/docker-scientific-python>`_
    is an image with `Alpine Linux <https://alpinelinux.org>`_ plus |glibc|
    library with Python 3.8.2 and modules. Although there are people who
    `do not suggest Alpine with Python <https://medium.com/@lih.verma/alpine-makes-python-docker-builds-way-too-50-slower-and-images-double-2-larger-61d1d43cbc79>`_,
    since this is the first version and its simplicity in construction,
    this container will be used. We encourage you to use it. The author
    will run the programs in this environment.

    .. code:: console

      [user@hostname ~]$ git clone https://github.com/carlosal1015/docker-scientific-python
      [user@hostname ~]$ cd docker-scientific-python
      [user@hostname ~]$ docker-compose up -d

    or

    .. code:: console

      [user@hostname ~]$ docker run -it --name scientific-python \
                                 -v $(PWD):/notebooks \
                                 -p 8888:8888 -d \
                                 carlosal1015/docker-scientific-python

    .. figure:: _static/img/docker-scientific-python.png
      :align: center
      :alt: Docker Scientific Python

  .. tab:: |macos|

    Omit the first two steps if already have installed |brew| package
    manager |:smile:|. For more details to install this manager and
    how to upgrade Python safely look
    `this post <https://www.digitalocean.com/community/tutorials/how-to-install-python-3-and-set-up-a-local-programming-environment-on-macos>`_.

    .. code-block:: console
      :linenos:
      :emphasize-lines: 1,2

      Users-name:~ User$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
      Users-name:~ User$ export PATH="/usr/local/opt/python/libexec/bin:$PATH" >> ~/.profile
      Users-name:~ User$ brew install python

  .. tab:: |windows|

    To enable us to use the Python interpreter, the text editors like
    |nano|, vim or emacs, the version control system |git| and the new
    **Windows Subsystem for Linux 2**, to name a few, as in a
    `Unix-like <https://en.wikipedia.org/wiki/Unix-like>`_ system it is
    recommended to install the `new product <https://devblogs.microsoft.com/commandline/introducing-windows-terminal>`_
    in the operating system **updated**. :download:`Download from the store <https://www.microsoft.com/en-us/p/windows-terminal-preview/9n0dx20hk701>`.
    `Cygwin <https://www.cygwin.com>`_ and `Cmder <https://cmder.net>`_
    are good too.

    .. tabs::

      .. tab:: |windows-terminal|

        .. youtube:: 8gw0rXPMMPE

        |powershell|

        If wants verify the integrity of the Python's installer, i.e.
        the contents of the file have not been changed and have
        `PowerShell <https://docs.microsoft.com/en-us/powershell>`_
        version 4 or later, please open a PowerShell console and
        check the version as follows:

        .. code:: ps1con

            PS C:\> $PSVersionTable.PSVersion # Check PowerShell version

            Major  Minor  Patch  PreReleaseLabel BuildLabel
            -----  -----  -----  --------------- ----------
            7      0      0

        .. note::

          Can choose a web-based (online installation) or executable
          installer (offline installation) for Python, the details are
          in `this guide <https://helpdeskgeek.com/how-to/how-to-use-python-on-windows>`_
          to install Python 3. `More info <https://docs.python.org/3/using/windows.html#installing-without-ui>`_.

        .. code:: ps1con

            PS C:\> $source = "https://www.python.org/ftp/python/3.8.2/python-3.8.2-amd64.exe"
            PS C:\> $destination = "$PSScriptRoot/python-3.8.2-amd64.exe"
            PS C:\> Invoke-WebRequest -Uri $source -OutFile $destination

        Then, we use the cmdlet |get-filehash| to computes the hash
        value, a hash assigns a unique value to the contents of a file.

        .. code:: ps1con

            PS C:\> Get-FileHash -? # Help for Get-FileHash

            NAME
                Get-FileHash

            SYNTAX
                Get-FileHash [-Path] <string[]> [[-Algorithm] {SHA1 | SHA256 | SHA384 | SHA512 | MD5}] [<CommonParameters>]

                Get-FileHash [-LiteralPath] <string[]> [[-Algorithm] {SHA1 | SHA256 | SHA384 | SHA512 | MD5}] [<CommonParameters>]

                Get-FileHash [-InputStream] <Stream> [[-Algorithm] {SHA1 | SHA256 | SHA384 | SHA512 | MD5}] [<CommonParameters>]

        .. code:: ps1con

            PS C:\> Get-FileHash ./python-3.8.2-amd64.exe -Algorithm MD5 | Format-List

            Algorithm : MD5
            Hash      : B5DF1CBB2BC152CD70C3DA9151CB510B
            Path      : C:\python-3.8.2-amd64.exe

        Alright, the hash value matches as seen in the image below.

        .. figure:: _static/img/python-windows.png
          :align: center
          :alt: Python for Windows.

        .. code:: ps1con

            PS C:\> $args = @("Shortcuts=0","Include_launcher=1","InstallLauncherAllUsers=1")
            PS C:\> Start-Process -Filepath "$PSScriptRoot/python-3.8.2-amd64.exe" -ArgumentList $args

        or for example

        .. code:: ps1con

            PS C:\> $installer = "C:\python-3.8.2-amd64.exe"
            PS C:\> & $installer /passive InstallAllUsers=1 PrependPath=1 Include_test=0

        If instead you want to install Python in graphical mode, then
        open the ``python-3.8.2-amd64.exe`` and follow carefully the
        images bellow, then will have Python installed without problems.

        .. figure:: _static/img/python-windows-installer-1.png
          :align: center
          :alt: Python for Windows.

        .. figure:: _static/img/python-windows-installer-2.png
          :align: center
          :alt: Python for Windows.

        .. figure:: _static/img/python-windows-installer-3.png
          :align: center
          :alt: Python for Windows.

      .. tab:: |wsl2|

        Once installed the new Windows terminal with success, then please
        follow the instructions since step four from the
        `documentation <https://docs.microsoft.com/en-us/windows/wsl/wsl2-install>`_
        or follow this friendly
        `guide <https://scotch.io/bar-talk/trying-the-new-wsl-2-its-fast-windows-subsystem-for-linux>`_.

        .. youtube:: ilKQHAFeQR0

        Finally, download your Linux flavor and there install Python as in Linux.

        .. hlist::
            :columns: 2

            * :download:`Ubuntu 18.04 LTS <https://www.microsoft.com/en-us/p/ubuntu-1804-lts/9n9tngvndl3q>`.
            * :download:`openSUSE Leap 15-1 <https://www.microsoft.com/en-us/p/opensuse-leap-15-1/9njfzk00fgkv>`.
            * :download:`SUSE Linux Enterprise Server 15 SP1 <https://www.microsoft.com/en-us/p/suse-linux-enterprise-server-15-sp1/9pn498vpmf3z>`.
            * :download:`Kali Linux <https://www.microsoft.com/en-us/p/kali-linux/9pkr34tncv07>`.
            * :download:`Debian GNU/Linux <https://www.microsoft.com/en-us/p/debian/9msvkqc78pk6>`.
            * :download:`Alpine WSL <https://www.microsoft.com/en-us/p/alpine-wsl/9p804crf0395>`.
            * :download:`ArchWSL <https://github.com/yuk7/ArchWSL>` and `more <https://github.com/yuk7/wsldl>`_.

      .. tab:: |chocolatey|

        First install `Chocolatey <https://chocolatey.org/docs/installation>`_
        and install `Python <https://chocolatey.org/packages/python/3.8.2>`_
        since PowerShell.

        .. code:: ps1con

          PS C:\> choco install python

.. |linux| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/tux.svg"><br/>
  </div>
  <div style="display:inline-block;">
    <h3 style="width:auto;">
      <a target="_blank" rel="noopener noreferrer" href="https://www.linuxfoundation.org/projects/linux">
      Linux-based system
      </a>
    </h3>
  </div>

.. |docker| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/docker.svg"><br/>
  </div>
  <div style="display:inline-block;">
    <h3 style="width:auto;">
      <a target="_blank" rel="noopener noreferrer" href="https://www.docker.com/play-with-docker">
      Docker</a>
    </h3>
  </div>

.. |glibc| raw:: html

  <tt><a href="https://www.gnu.org/software/libc">glibc</a></tt>

.. |macos| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/mac.svg"><br/>
  </div>
  <div style="display:inline-block;">
    <h3 style="width:auto;">
      macOS
    </h3>
  </div>

.. |brew| raw:: html

  <tt><a href="https://brew.sh">brew</a></tt>

.. |windows| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/microsoft-logo.png"><br/>
  </div>
  <div style="display:inline-block;">
    <h3 style="width:auto;">
      Windows 10
    </h3>
  </div>

.. |nano| raw:: html

  <tt><a href="https://www.nano-editor.org">nano</a></tt>

.. |git| raw:: html

   <tt><a href="https://git-scm.com">git</a></tt>

.. |powershell| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/powershell-logo.png"><br/>
  </div>
  <div style="display:inline-block;">
    <h4 style="width:auto;">
      <a target="_blank" rel="noopener noreferrer" href="https://github.com/powershell/powershell">
      PowerShell</a>
    </h4>
  </div>


.. |windows-terminal| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/windows-terminal-logo.png"><br/>
  </div>
  <div style="display:inline-block;">
    <h4 style="width:auto;">
      Windows Terminal
    </h4>
  </div>

.. |wsl2| raw:: html

  <div style="display:inline-block;">
    <h4 style="width:auto;">
      <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/windows/wsl">
      Windows Subystem for Linux 2</a>
    </h4>
  </div>

.. |get-filehash| raw:: html

   <tt><a href="https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-filehash?view=powershell-7">Get-FileHash</a></tt>

.. |chocolatey| raw:: html

  <div style="display:inline-block;">
    <img style="width:38px;" src="_static/img/chocolatey-logo.svg"><br/>
  </div>
  <div style="display:inline-block;">
    <h4 style="width:auto;">
      <a target="_blank" rel="noopener noreferrer" href="https://chocolatey.org">
      Chocolatey</a>
    </h4>
  </div>

.. tabs::

  .. tab:: |pip3|

    Install the package (or add it to your |requirements.txt| file |:grinning:|):

    .. code:: console

        [user@hostname ~]$ pip install matplotlib # numpy it's included

    or

    .. code:: console

        [user@hostname ~]$ pip install -r requirements.txt

  .. tab:: |conda|

    In the `Introductory Autumn School on Scientific Computing <https://www.pec3.org/index.php?show=events_internationalconferenceonscientificcomputing_school&lang=en>`_
    they advices us to install `Anaconda distribution <https://www.anaconda.com>`_.

    On macOS system, open the terminal emulator and put:

    .. code:: console

        Users-name:~ User$ curl -sfL https://git.io/Jv91a | sh -

    On `GNU/Linux <https://www.gnu.org/gnu/linux-and-gnu.en.html>`_ or WSL2,
    open the terminal emulator and put:

    .. code:: console

        [user@hostname ~]$ curl -sfL https://git.io/Jv91R | sh -
        [user@hostname ~]$ yay -Sy miniconda3 # Arch Linux, Manjaro

    After the conda installation, please install |matplotlib| in a new
    environment.

    .. code:: console

        [user@hostname ~]$ conda create -n scientificpython python=3.8 matplotlib
        [user@hostname ~]$ conda activate scientificpython

    .. warning::

      The instruction ``curl -flags URL | sh`` is a direct way to execute code over the internet,
      always examine the script before to execute and only trusted from secure sites.

    .. seealso::

        .. hlist::
          :columns: 2

          * `Intel® Distribution for Python <https://software.intel.com/en-us/articles/using-intel-distribution-for-python-with-anaconda>`_.
          * `Docker Images for Intel® Distribution for Python <https://software.intel.com/en-us/articles/docker-images-for-intel-python>`_.

.. |pip3| raw:: html

  <div style="display:inline-block;">
    <img style="width:100px;" src="_static/img/pip-logo.png"><br/>
  </div>
  <div style="display:inline-block;">
    <h3 style="width:auto;">
      (Pip Installs Packages)
    </h3>
  </div>

.. |requirements.txt| raw:: html

   <tt><a href="https://docs.python.org/3/tutorial/venv.html">requirements.txt</a></tt>

.. note::

    Python has several package administrators, such as |pip|, and some
    others integrate a `virtual environment <https://docs.python.org/3/glossary.html#term-virtual-environment>`_.
    A brief explanation of the most popular managers is found `here <https://invidio.us/watch?v=3J02sec99RM>`_.

.. |pip| raw:: html

   <tt><a href="https://pip.pypa.io/en/stable">pip</a></tt>

.. |conda| raw:: html

    <div style="display:inline-block;">
      <img style="width:100px;" src="_static/img/conda-logo.svg"><br/>
    </div>
    <div style="display:inline-block;">
      <h3 style="width:auto;">
        management
      </h3>
    </div>

.. |matplotlib| raw:: html

   <tt><a href="https://matplotlib.org">matplotlib</a></tt>


.. figure:: _static/img/learn-linux.png
  :figwidth: 750 px
  :alt: Alternative text
  :align: center

.. figure:: _static/img/learn-linux-2.png
  :figwidth: 750 px
  :alt: Alternative text
  :align: center
