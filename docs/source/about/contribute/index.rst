.. _about-user-contribute:

##############
  Contribute
##############

If you find an error in the documentation, please
`report the problem <https://gitlab.com/carlosal1015/introduction2scientificpython/issues>`_.

.. _about-getting-started:

Getting Started
===============

The following guides lead you through the process.

.. toctree::
   :maxdepth: 1

   install/index.rst
   build/index.rst
   editing.rst
   community.rst

Guidelines
==========

.. toctree::
   :maxdepth: 1

   guides/writing_guide.rst
   guides/markup_guide.rst
   guides/maintenance_guide.rst

Translations
============

.. toctree::
   :maxdepth: 1

   translations/contribute.rst
   translations/style_guide.rst

.. _contribute-contact:

Contacts
========

`project page <https://gitlab.com/carlosal1015/introduction2scientificpython/>`_.
   An overview of the documentation project.
email address `drc@lamolina.edu.pe <mailto:drc@lamolina.edu.pe>`_ `kaya@math.uni-kiel.de <mailto:kaya@math.uni-kiel.de>`_ `caznaranl@uni.pe <mailto:caznaranl@uni.pe>`_
   A mailing list for discussing ideas, and keeping track of progress.
:ref:`gitter-chat`
   ``#docs`` channel for informal discussions in real-time.
