.. highlight:: console

*********************
Installation on macOS
*********************

This guide covers the following topics:

#. `Installing Dependencies`_
#. `Downloading the Repository`_
#. `Setting up the Build Environment`_

.. note::

   This guide relies heavily on command-line tools.
   It assumes you are the least familiar with the macOS Terminal application.


Installing Dependencies
=======================

Install those packages or make sure you have them in your system.

- `Git <https://git-scm.com>`_
- `Python <https://www.python.org>`_
- `PIP <https://pip.pypa.io/en/latest/installing>`_
- `Pandoc <https://pandoc.org>`_
- `Graphviz <https://www.graphviz.org>`_
- `Poetry <https://python-poetry.org>`_


Downloading the Repository
==========================

Simply check out the ScientificPython's repository using::

      Users-name:~ User$ git clone --depth=1 https://gitlab.com/carlosal1015/introduction2scientificpython.git

The repository will now be downloaded which may take a few minutes depending on your internet connection.


Setting up the Build Environment
================================

- Open a Terminal window.
- Enter the ``~/introduction2scientificpython`` folder which was just added by the git::

      Users-name:~ User$ cd introduction2scientificpython/docs
      Users-name:~ User$ poetry install -E docs --no-dev

- Inside that folder is a file called ``requirements.txt`` which contains a list of all the dependencies we need.
  To install these dependencies, we can use the ``pip`` command::

      Users-name:~ User$ python -m venv my-venv
      Users-name:~ User$ source my-venv/bin/activate
      Users-name:~ User$ pip install -r requirements.txt

.. note::

   Every now and then you may want to make sure your dependencies are up to date using::

      Users-name:~ User$ source my-venv/bin/activate
      Users-name:~ User$ pip install -r requirements.txt --upgrade


------------------------

Continue with the next step: :doc:`Building </about/contribute/build/macos>`.
