.. highlight:: console

*********************
Installation on Linux
*********************

This guide covers the following topics:

#. `Installing Dependencies`_
#. `Downloading the Repository`_
#. `Setting up the Build Environment`_


Installing Dependencies
=======================

Below are listed the installation commands for popular Linux distributions.

For the appropriate system, run the command in a terminal:

Debian/Ubuntu
   .. code-block:: console

      [user@hostname ~]$ sudo apt install git python-dev pandoc graphviz
      [user@hostname ~]$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
      [user@hostname ~]$ export POETRY_VIRTUALENVS_IN_PROJECT=true >> ~/.zshrc
      [user@hostname ~]$ source ~/.zshrc

Redhat/Fedora
   .. code-block:: console

      [user@hostname ~]$ sudo yum install git python-devel pandoc graphviz
      [user@hostname ~]$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
      [user@hostname ~]$ export POETRY_VIRTUALENVS_IN_PROJECT=true >> ~/.zshrc
      [user@hostname ~]$ source ~/.zshrc

openSUSE/SUSE
   .. code-block:: console

      [user@hostname ~]$ sudo zypper install git python-devel pandoc graphviz
      [user@hostname ~]$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
      [user@hostname ~]$ export POETRY_VIRTUALENVS_IN_PROJECT=true >> ~/.zshrc
      [user@hostname ~]$ source ~/.zshrc

Arch Linux/Manjaro
   .. code-block:: console

      [user@hostname ~]$ sudo pacman -Sy git poetry pandoc graphviz
      [user@hostname ~]$ export POETRY_VIRTUALENVS_IN_PROJECT=true >> ~/.zshrc
      [user@hostname ~]$ source ~/.zshrc


Downloading the Repository
==========================

Simply check out the ScientificPython's repository using::

      [user@hostname ~]$ git clone --depth=1 https://gitlab.com/carlosal1015/introduction2scientificpython.git

The repository will now be downloaded which may take a few minutes depending on your internet connection.


Setting up the Build Environment
================================

- Open a Terminal window.
- Enter the ``~/introduction2scientificpython`` folder which was just added by git::

      [user@hostname ~]$ cd introduction2scientificpython
      [user@hostname ~]$ poetry install -E docs --no-dev

- Inside that folder is a file called ``requirements.txt`` which contains a list of all the dependencies we need.
  To install these dependencies, we can use the ``pip`` command::

      [user@hostname ~]$ python -m venv my-venv
      [user@hostname ~]$ source my-venv/bin/activate
      [user@hostname ~]$ pip install -r requirements.txt

.. note::

   Every now and then you may want to make sure your dependencies are up to date using::

      [user@hostname ~]$ source my-venv/bin/activate
      [user@hostname ~]$ pip install -r requirements.txt --upgrade


------------------------

Continue with the next step: :doc:`Building </about/contribute/build/linux>`.
