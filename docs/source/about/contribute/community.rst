********************
The Python Community
********************

Being freely available from the start, even while it was closed source,
considerably helped Python's adoption by the community.

Independent Sites
=================

There are `several independent websites <https://realpython.com>`_
such as forums, blogs, news, and tutorial sites dedicated to Python.

Getting Support
===============

Python's community is one of its greatest features, so apart from this user manual,
there are many different ways to get support from other users, such as :ref:`gitter-chat`
and `Stack Exchange <https://stackoverflow.com/questions/tagged/python>`_.

If you think you have found an issue with ``introduction2scientificpython``, you can easily
`report a bug <https://gitlab.com/carlosal1015/introduction2scientificpython/-/issues>`_.

Development
===========

Being open source, ``introduction2scientificpython`` welcomes development from volunteers.
Communication between developers is done mostly through three platforms:

- Email address
- Online Chat (see below)
- Issue's section from the GitLab repository

.. _gitter-chat:

Gitter Chat
===========

For real-time discussion, you can join these channels:

- `#python <https://gitter.im/carlosal1015-introduction2scientificpython/python>`_
  For support for developers using the Python API.
- `#docs <https://gitter.im/carlosal1015-introduction2scientificpython/docs>`_
  For discussion related to Blender's documentation.


Other Useful Links
==================

- `Python FAQ <https://docs.python.org/3/faq/>`_ (Why is Python installed on my Computer? ...)
